# 前端的运行

# 一、前端的运行

## 1.1 安装node.js

### 1.1.1 下载node.js

![1576565889320](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576565889320.png)

可以在网上下载：

https://npm.taobao.org/mirrors/node/v10.16.0/node-v10.16.0-win-x64.zip

### 1.1.2 安装node.js

![1576566065534](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566065534.png)

复制压缩包：

![1576566141837](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566141837.png)

配置环境变量：

![1576566224334](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566224334.png)



![1576566283094](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566283094.png)

使用cmd 试运行node 程序：

![1576566339451](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566339451.png)

版本必须为10.16.0

## 1.2 配置npm 下载的加速地址

npm 和maven 比较像，npm 也是管理前端的js 包！

要配置为淘宝的加速地址：

你可以使用我们定制的 [cnpm](https://github.com/cnpm/cnpm) (gzip 压缩支持) 命令行工具代替默认的 `npm`:

```
 npm install -g cnpm --registry=https://registry.npm.taobao.org
```

![1576566485440](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566485440.png)



![1576566522188](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566522188.png)

## 1.3 运行vue的程序

### 1.3.1 下载前端的源码

![1576566583395](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566583395.png)

### 1.3.2 将下载的源码放在我们git 的文件夹里面

![1576566645900](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566645900.png)



![1576566743171](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566743171.png)



### 1.3.3 vue 的运行

![1576566799807](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566799807.png)

 安装依赖：

```bash
cnpm install
```

安装完成后：

![1576566959736](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576566959736.png)

![1576567022409](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576567022409.png)

运行前端：

```bash
 cnpm run dev
```

成功后：

将自动弹出 8001

## 1.4 微信小程序的运行

### 1.4.1 下载微信开发者工具

![1576569731777](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576569731777.png)

### 1.4.2 安装该软件

![1576569854798](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576569854798.png)



![1576569948921](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576569948921.png)



启动成功后：

![1576570003330](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576570003330.png)

![1576570029375](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576570029375.png)





![1576570113680](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576570113680.png)



点击测试号：

![1576570178513](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576570178513.png)

导入成功后：

![1576570283495](http://117.48.231.143/liangtiandong/ego-shop/raw/master/front/assert/1576570283495.png)