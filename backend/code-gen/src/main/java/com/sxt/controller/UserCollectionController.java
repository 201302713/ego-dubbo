package com.sxt.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@RestController
@RequestMapping("/user-collection")
public class UserCollectionController {

}
