package com.sxt.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 短信记录表 前端控制器
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@RestController
@RequestMapping("/sms-log")
public class SmsLogController {

}
