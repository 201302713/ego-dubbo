package com.sxt.mapper;

import com.sxt.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface BrandMapper extends BaseMapper<Brand> {

}
