package com.sxt.mapper;

import com.sxt.entity.ProdComm-compare-test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品评论 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdComm-compare-testMapper extends BaseMapper<ProdComm-compare-test> {

}
