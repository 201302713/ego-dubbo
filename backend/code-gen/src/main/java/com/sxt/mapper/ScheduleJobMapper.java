package com.sxt.mapper;

import com.sxt.entity.ScheduleJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {

}
