package com.sxt.mapper;

import com.sxt.entity.PickAddr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户配送地址 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface PickAddrMapper extends BaseMapper<PickAddr> {

}
