package com.sxt.mapper;

import com.sxt.entity.Transfee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface TransfeeMapper extends BaseMapper<Transfee> {

}
