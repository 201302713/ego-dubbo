package com.sxt.mapper;

import com.sxt.entity.UserAddrOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户订单配送地址 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface UserAddrOrderMapper extends BaseMapper<UserAddrOrder> {

}
