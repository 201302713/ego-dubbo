package com.sxt.mapper;

import com.sxt.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色与菜单对应关系 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
