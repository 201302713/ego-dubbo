package com.sxt.mapper;

import com.sxt.entity.SmsLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 短信记录表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SmsLogMapper extends BaseMapper<SmsLog> {

}
