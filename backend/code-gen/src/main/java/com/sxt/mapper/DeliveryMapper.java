package com.sxt.mapper;

import com.sxt.entity.Delivery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 物流公司 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface DeliveryMapper extends BaseMapper<Delivery> {

}
