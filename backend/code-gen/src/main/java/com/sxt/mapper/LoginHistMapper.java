package com.sxt.mapper;

import com.sxt.entity.LoginHist;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登录历史表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface LoginHistMapper extends BaseMapper<LoginHist> {

}
