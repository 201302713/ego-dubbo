package com.sxt.mapper;

import com.sxt.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单项 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
