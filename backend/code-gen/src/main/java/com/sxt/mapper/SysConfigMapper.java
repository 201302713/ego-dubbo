package com.sxt.mapper;

import com.sxt.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统配置信息表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
