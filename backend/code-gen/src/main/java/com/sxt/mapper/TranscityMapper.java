package com.sxt.mapper;

import com.sxt.entity.Transcity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface TranscityMapper extends BaseMapper<Transcity> {

}
