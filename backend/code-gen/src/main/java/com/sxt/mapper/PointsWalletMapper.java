package com.sxt.mapper;

import com.sxt.entity.PointsWallet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface PointsWalletMapper extends BaseMapper<PointsWallet> {

}
