package com.sxt.mapper;

import com.sxt.entity.OauthClientDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 终端信息表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails> {

}
