package com.sxt.mapper;

import com.sxt.entity.SensWord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 敏感字过滤表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SensWordMapper extends BaseMapper<SensWord> {

}
