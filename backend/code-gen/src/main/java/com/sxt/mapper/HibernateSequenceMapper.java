package com.sxt.mapper;

import com.sxt.entity.HibernateSequence;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface HibernateSequenceMapper extends BaseMapper<HibernateSequence> {

}
