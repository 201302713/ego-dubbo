package com.sxt.mapper;

import com.sxt.entity.ScheduleJobLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务日志 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ScheduleJobLogMapper extends BaseMapper<ScheduleJobLog> {

}
