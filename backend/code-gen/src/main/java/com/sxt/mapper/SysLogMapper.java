package com.sxt.mapper;

import com.sxt.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
