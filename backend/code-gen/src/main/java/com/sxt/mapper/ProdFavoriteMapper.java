package com.sxt.mapper;

import com.sxt.entity.ProdFavorite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品收藏表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdFavoriteMapper extends BaseMapper<ProdFavorite> {

}
