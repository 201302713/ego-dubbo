package com.sxt.service;

import com.sxt.entity.ProdTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分组表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdTagService extends IService<ProdTag> {

}
