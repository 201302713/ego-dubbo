package com.sxt.service.impl;

import com.sxt.entity.UserAddrOrder;
import com.sxt.mapper.UserAddrOrderMapper;
import com.sxt.service.UserAddrOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户订单配送地址 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class UserAddrOrderServiceImpl extends ServiceImpl<UserAddrOrderMapper, UserAddrOrder> implements UserAddrOrderService {

}
