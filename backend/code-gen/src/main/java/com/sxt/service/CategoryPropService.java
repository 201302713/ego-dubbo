package com.sxt.service;

import com.sxt.entity.CategoryProp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface CategoryPropService extends IService<CategoryProp> {

}
