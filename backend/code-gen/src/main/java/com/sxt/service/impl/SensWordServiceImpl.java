package com.sxt.service.impl;

import com.sxt.entity.SensWord;
import com.sxt.mapper.SensWordMapper;
import com.sxt.service.SensWordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 敏感字过滤表 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class SensWordServiceImpl extends ServiceImpl<SensWordMapper, SensWord> implements SensWordService {

}
