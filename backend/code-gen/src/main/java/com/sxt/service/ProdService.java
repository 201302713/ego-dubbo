package com.sxt.service;

import com.sxt.entity.Prod;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdService extends IService<Prod> {

}
