package com.sxt.service;

import com.sxt.entity.IndexImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 主页轮播图 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface IndexImgService extends IService<IndexImg> {

}
