package com.sxt.service;

import com.sxt.entity.PointsChange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface PointsChangeService extends IService<PointsChange> {

}
