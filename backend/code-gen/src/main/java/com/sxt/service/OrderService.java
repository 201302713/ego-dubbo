package com.sxt.service;

import com.sxt.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderService extends IService<Order> {

}
