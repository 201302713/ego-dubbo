package com.sxt.service;

import com.sxt.entity.LoginHist;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登录历史表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface LoginHistService extends IService<LoginHist> {

}
