package com.sxt.service;

import com.sxt.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysMenuService extends IService<SysMenu> {

}
