package com.sxt.service;

import com.sxt.entity.ScheduleJobLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 定时任务日志 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLog> {

}
