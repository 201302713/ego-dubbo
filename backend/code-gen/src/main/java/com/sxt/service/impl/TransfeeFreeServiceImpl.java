package com.sxt.service.impl;

import com.sxt.entity.TransfeeFree;
import com.sxt.mapper.TransfeeFreeMapper;
import com.sxt.service.TransfeeFreeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class TransfeeFreeServiceImpl extends ServiceImpl<TransfeeFreeMapper, TransfeeFree> implements TransfeeFreeService {

}
