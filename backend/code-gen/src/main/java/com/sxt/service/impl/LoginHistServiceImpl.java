package com.sxt.service.impl;

import com.sxt.entity.LoginHist;
import com.sxt.mapper.LoginHistMapper;
import com.sxt.service.LoginHistService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录历史表 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class LoginHistServiceImpl extends ServiceImpl<LoginHistMapper, LoginHist> implements LoginHistService {

}
