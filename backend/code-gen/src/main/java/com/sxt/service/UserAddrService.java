package com.sxt.service;

import com.sxt.entity.UserAddr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户配送地址 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface UserAddrService extends IService<UserAddr> {

}
