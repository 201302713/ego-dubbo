package com.sxt.service.impl;

import com.sxt.entity.ProdProp;
import com.sxt.mapper.ProdPropMapper;
import com.sxt.service.ProdPropService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class ProdPropServiceImpl extends ServiceImpl<ProdPropMapper, ProdProp> implements ProdPropService {

}
