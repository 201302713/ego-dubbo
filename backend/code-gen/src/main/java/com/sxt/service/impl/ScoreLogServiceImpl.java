package com.sxt.service.impl;

import com.sxt.entity.ScoreLog;
import com.sxt.mapper.ScoreLogMapper;
import com.sxt.service.ScoreLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class ScoreLogServiceImpl extends ServiceImpl<ScoreLogMapper, ScoreLog> implements ScoreLogService {

}
