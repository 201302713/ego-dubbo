package com.sxt.service;

import com.sxt.entity.ScheduleJob;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 定时任务 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ScheduleJobService extends IService<ScheduleJob> {

}
