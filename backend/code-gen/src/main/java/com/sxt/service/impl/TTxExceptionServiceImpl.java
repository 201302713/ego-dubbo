package com.sxt.service.impl;

import com.sxt.entity.TTxException;
import com.sxt.mapper.TTxExceptionMapper;
import com.sxt.service.TTxExceptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class TTxExceptionServiceImpl extends ServiceImpl<TTxExceptionMapper, TTxException> implements TTxExceptionService {

}
