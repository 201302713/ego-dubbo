package com.sxt.service.impl;

import com.sxt.entity.HibernateSequence;
import com.sxt.mapper.HibernateSequenceMapper;
import com.sxt.service.HibernateSequenceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class HibernateSequenceServiceImpl extends ServiceImpl<HibernateSequenceMapper, HibernateSequence> implements HibernateSequenceService {

}
