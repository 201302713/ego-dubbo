package com.sxt.service;

import com.sxt.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysUserService extends IService<SysUser> {

}
