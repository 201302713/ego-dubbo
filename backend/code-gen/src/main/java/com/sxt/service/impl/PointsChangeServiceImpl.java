package com.sxt.service.impl;

import com.sxt.entity.PointsChange;
import com.sxt.mapper.PointsChangeMapper;
import com.sxt.service.PointsChangeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class PointsChangeServiceImpl extends ServiceImpl<PointsChangeMapper, PointsChange> implements PointsChangeService {

}
