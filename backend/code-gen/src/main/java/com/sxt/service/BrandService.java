package com.sxt.service;

import com.sxt.entity.Brand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 品牌表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface BrandService extends IService<Brand> {

}
