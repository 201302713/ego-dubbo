package com.sxt.service;

import com.sxt.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 产品类目 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface CategoryService extends IService<Category> {

}
