package com.sxt.service.impl;

import com.sxt.entity.PointsProd;
import com.sxt.mapper.PointsProdMapper;
import com.sxt.service.PointsProdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class PointsProdServiceImpl extends ServiceImpl<PointsProdMapper, PointsProd> implements PointsProdService {

}
