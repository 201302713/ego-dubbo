package com.sxt.service.impl;

import com.sxt.entity.PointsWallet;
import com.sxt.mapper.PointsWalletMapper;
import com.sxt.service.PointsWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class PointsWalletServiceImpl extends ServiceImpl<PointsWalletMapper, PointsWallet> implements PointsWalletService {

}
