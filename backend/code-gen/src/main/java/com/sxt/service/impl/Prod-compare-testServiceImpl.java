package com.sxt.service.impl;

import com.sxt.entity.Prod-compare-test;
import com.sxt.mapper.Prod-compare-testMapper;
import com.sxt.service.Prod-compare-testService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class Prod-compare-testServiceImpl extends ServiceImpl<Prod-compare-testMapper, Prod-compare-test> implements Prod-compare-testService {

}
