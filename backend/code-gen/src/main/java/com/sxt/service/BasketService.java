package com.sxt.service;

import com.sxt.entity.Basket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface BasketService extends IService<Basket> {

}
