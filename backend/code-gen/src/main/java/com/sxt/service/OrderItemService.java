package com.sxt.service;

import com.sxt.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单项 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderItemService extends IService<OrderItem> {

}
