package com.sxt.service;

import com.sxt.entity.SmsLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 短信记录表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SmsLogService extends IService<SmsLog> {

}
