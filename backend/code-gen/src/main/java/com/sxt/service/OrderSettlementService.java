package com.sxt.service;

import com.sxt.entity.OrderSettlement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderSettlementService extends IService<OrderSettlement> {

}
