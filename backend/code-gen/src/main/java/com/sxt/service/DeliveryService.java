package com.sxt.service;

import com.sxt.entity.Delivery;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 物流公司 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface DeliveryService extends IService<Delivery> {

}
