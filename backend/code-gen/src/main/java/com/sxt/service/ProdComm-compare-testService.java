package com.sxt.service;

import com.sxt.entity.ProdComm-compare-test;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品评论 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdComm-compare-testService extends IService<ProdComm-compare-test> {

}
