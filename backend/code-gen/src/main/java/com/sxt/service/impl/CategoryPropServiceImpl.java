package com.sxt.service.impl;

import com.sxt.entity.CategoryProp;
import com.sxt.mapper.CategoryPropMapper;
import com.sxt.service.CategoryPropService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class CategoryPropServiceImpl extends ServiceImpl<CategoryPropMapper, CategoryProp> implements CategoryPropService {

}
