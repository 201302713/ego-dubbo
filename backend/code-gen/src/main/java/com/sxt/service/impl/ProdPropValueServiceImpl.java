package com.sxt.service.impl;

import com.sxt.entity.ProdPropValue;
import com.sxt.mapper.ProdPropValueMapper;
import com.sxt.service.ProdPropValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class ProdPropValueServiceImpl extends ServiceImpl<ProdPropValueMapper, ProdPropValue> implements ProdPropValueService {

}
