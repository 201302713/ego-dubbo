package com.sxt.service.impl;

import com.sxt.entity.TranscityFree;
import com.sxt.mapper.TranscityFreeMapper;
import com.sxt.service.TranscityFreeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class TranscityFreeServiceImpl extends ServiceImpl<TranscityFreeMapper, TranscityFree> implements TranscityFreeService {

}
