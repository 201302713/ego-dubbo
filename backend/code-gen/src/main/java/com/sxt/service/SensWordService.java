package com.sxt.service;

import com.sxt.entity.SensWord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 敏感字过滤表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SensWordService extends IService<SensWord> {

}
