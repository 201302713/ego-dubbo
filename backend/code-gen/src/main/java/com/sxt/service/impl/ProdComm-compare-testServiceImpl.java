package com.sxt.service.impl;

import com.sxt.entity.ProdComm-compare-test;
import com.sxt.mapper.ProdComm-compare-testMapper;
import com.sxt.service.ProdComm-compare-testService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品评论 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class ProdComm-compare-testServiceImpl extends ServiceImpl<ProdComm-compare-testMapper, ProdComm-compare-test> implements ProdComm-compare-testService {

}
