package com.sxt.service;

import com.sxt.entity.OauthClientDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 终端信息表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OauthClientDetailsService extends IService<OauthClientDetails> {

}
