package com.sxt.vo.basket;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 直接对前端返回的值
 * @author WHSXT-LTD
 *
 */
@Data
public class CartResultVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 购物车里面的商品详情
	 */
	private List<ShopCartItemDiscount> shopCartItemDiscounts = Collections.emptyList();
	/**
	 * 订单的运费
	 */
	@ApiModelProperty("订单的运费")
	private BigDecimal transfee ;
	/**
	 * 订单的优惠金额
	 */
	@ApiModelProperty("订单的优惠金额")
	private BigDecimal shopReduce ;
}
