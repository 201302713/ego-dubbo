package com.sxt.vo.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import com.sxt.entity.UserAddr;
import com.sxt.vo.basket.CartResultVo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * OrderVo->res->
 *  res.shopCartOrders[0].shopCartItemDiscounts
 *  CartResultVo:shopCartItemDiscounts
 *    
 * @author WHSXT-LTD
 *
 */
@Data
public class OrderVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * 购物车里面的条目
	 */
	private List<CartResultVo> shopCartOrders = Collections.emptyList();
	

	@ApiModelProperty("订单的实际金额")
	private BigDecimal actualTotal ;
	
	
	@ApiModelProperty("订单的总金额")
	private BigDecimal total ;
	
	@ApiModelProperty("订单里面的商品数据")
	private Integer totalCount ;
	
	@ApiModelProperty("订单的收货地址")
	private UserAddr userAddr ;
	
	
	@ApiModelProperty("订单的编号")
	private String orderSn ;
	
}
