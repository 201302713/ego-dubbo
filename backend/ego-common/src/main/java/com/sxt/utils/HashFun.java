package com.sxt.utils;

public class HashFun {

	private int seed ;

	public HashFun(int seed) {
		this.seed = seed ;
	}

	/**
	 * 得到一个hashcode
	 * @param valueStr
	 * @return
	 */
	public int hashCode(String valueStr) {
		int h = 0 ;
		char[] value = valueStr.toCharArray();
		if (h == 0 && value.length > 0) {
			char val[] = value;

			for (int i = 0; i < value.length; i++) {
				h = seed * h + val[i];
			}
		}
		return h ; 
	}
}
