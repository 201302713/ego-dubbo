package com.sxt.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 惰性删除--->服务器不主动删除
 * @author WHSXT-LTD
 * 1 惰性删除的特点
 * 缺点:
 *  必须有用户访问该key,该key才会触发删除,若用户一直不访问 ,则该key 一直存在,浪费内存资源
 *优点:
 * 不需要删除的线程
 *  
 * 
 *
 */
public class RedisDeletePolicy2 {

	public static Map<String,String> redis = new HashMap<String, String>();

	/**
	 * String: key
	 * Long: 未来它要死的时间
	 */
	public static Map<String,Long> expires = new HashMap<>();
	/**
	 * 放入key和value,并且设置过期时间
	 * @param key
	 * @param value
	 * @param timeout
	 */
	public static void set(String key,String value,long timeout) {
		redis.put(key, value);
		expires.put(key, System.currentTimeMillis() + timeout); // 未来你死的时间

	}

	/**
	 * 从redis 里面获取值
	 * @param key
	 * @return
	 */
	public static String get(String key) {
		if(expires.containsKey(key)) {
			Long futureDeadTime = expires.get(key);
			if(System.currentTimeMillis() >= futureDeadTime) { // 你已经死了
                 redis.remove(key);
                 expires.remove(key) ;
                 return null ;
			}
		}
		return redis.get(key);
	}
	public static void main(String[] args) throws InterruptedException {
		set("test", "value", 5000);
		Thread.sleep(2000);
		String exisKey = get("test");
		System.out.println(exisKey);
		Thread.sleep(3001);
		System.out.println(get("test")); // 5s 后删除了
	}
}
