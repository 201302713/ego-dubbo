package com.sxt.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 定期删除
 * 服务器使用一个线程去主动删除过期的key
 * @author WHSXT-LTD
 * 定期删除
 * 使用一个线程去主动删除过期的key
 * 1 缺点:
 *   就是删除的实时性不高
 *   redis 能设置为naos 级别
 * 2 优点:
 *  需要一个线程 cpu
 * 内存也不浪费  mem
 * 
 * 3 实时性问题使用惰性删除解决
 *
 */
public class RedisDeletePolicy3 {

	private static Map<String,String> redis = new HashMap<String,String>();

	/**
	 * Long:代表未来它死的时间
	 */
	public static Map<String,Long> expires = new HashMap<>();

	static {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Set<String> keys = expires.keySet();
					Set<String> expiresKeys = new HashSet<String>(); // 过期的值
					for (String key : keys) {
						Long futureDeadTime = expires.get(key);
						if(System.currentTimeMillis() >=futureDeadTime ) {
							expiresKeys.add(key);
						}
					}
					for (String expireKey : expiresKeys) {
						redis.remove(expireKey);
						expires.remove(expireKey);
					}

				}
			}
		}).start();
	}

	public static void set(String key,String value,long timeout) {
		redis.put(key, value);
		expires.put(key, System.currentTimeMillis() + timeout) ;
	}

	public static String get(String key) {
		/**
		 * 解决实时性问题
		 */
		if(expires.containsKey(key)) {
			Long futrueDeadTime = expires.get(key);
			if(System.currentTimeMillis() >= futrueDeadTime) {
				redis.remove(key);
				expires.remove(key);
				return null ;
			}
		}

		return redis.get(key);
	}

	public static void main(String[] args) throws InterruptedException {
		set("test", "value", 5000);
		while (true) {
			Thread.sleep(1000);
			System.out.println(get("test"));
		}
	}
}
