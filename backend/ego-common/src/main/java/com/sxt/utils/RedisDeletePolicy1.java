package com.sxt.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 定时删除
 * @author WHSXT-LTD
 * 给每个key 都设置一个线程,该线程会休眠一段时间,然后将它删除
 * 1 每个key 都需要一个线程
 * 优点:
 * 实时性高
 * 缺点:
 * 浪费线程的资源,cpu的资源
 *
 */
public class RedisDeletePolicy1 {
//1 模拟redis
	private static Map<String,String> redis  = new HashMap<String,String>();


  /**
   * 给redis 里面放一个值,并且设置过期时间
   */
	public static void set(String key,String value ,long timeout) {
		redis.put(key, value);
		// 该线程就是专门来移除该过期key值得
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(timeout);
					redis.remove(key);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
	}
	/**
	 * 获取值
	 * @param key
	 * @return
	 */
	public static String get(String key) {
		return redis.get(key);
	}
	public static void main(String[] args) throws InterruptedException {
		set("test", "value", 5000);
		Thread.sleep(2000);
		String exisKey = get("test");
		System.out.println(exisKey);
		Thread.sleep(3001);
		System.out.println(get("test")); // 5s 后删除了
	}
	
}
