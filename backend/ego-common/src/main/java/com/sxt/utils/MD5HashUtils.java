package com.sxt.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * md5 hash的工具类
 * @author WHSXT-LTD
 *
 */
public class MD5HashUtils {

	/**
	 * 得到加密后的字符串
	 * @param oriStr
	 * @param salt
	 * @param count
	 * @return
	 */
	public static String toHex(String oriStr,String salt,Integer count) {
		Md5Hash md5Hash = new Md5Hash(oriStr, salt, count);
		return md5Hash.toString();
	}
}
