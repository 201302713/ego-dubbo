package com.sxt.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


/**
 * 使用jdk的序列化实现对象的编码
 * @author WHSXT-LTD
 *
 */
public class JDKCoderImpl {


	public static byte[] code(Object object) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = null ;
		try {
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			// 将对象写入到字节输出流里面
			objectOutputStream.writeObject(object);
			// 写完之后，最好有flush
			objectOutputStream.writeByte(-1);
			objectOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			// 关闭资源
		}
		// 从字节输出流里面得到字节数组
		byte[] bs = byteArrayOutputStream.toByteArray();
		return bs;
	}

}
