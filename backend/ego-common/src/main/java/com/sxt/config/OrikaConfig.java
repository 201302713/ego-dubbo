package com.sxt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
public class OrikaConfig {

	/**
	 * 在ioc 容器里面注入	MapperFacade
	 * @return
	 */
	@Bean
	public MapperFacade mapperFacade() {
		return  new DefaultMapperFactory.Builder().build().getMapperFacade() ;
	}
}
