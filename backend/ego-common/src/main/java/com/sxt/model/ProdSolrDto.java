package com.sxt.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 导入到solr 里面的商品的数据
 * @author WHSXT-LTD
 *
 */
@Data
@Builder // 启用该类的构建者模式
@AllArgsConstructor // @Builder
@NoArgsConstructor
public class ProdSolrDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1085164392693760491L;

	@ApiModelProperty("商品的id")
	@Field
	private String id;

	@ApiModelProperty(value = "商品名称")
	@Field("prod_name")
	private String prodName;

	@ApiModelProperty(value = "现价")
	@Field
	private BigDecimal price;

	@ApiModelProperty(value = "简要描述,卖点等")
	@Field
	private String brief;
	
    @ApiModelProperty(value = "商品主图")
    @Field
    private String pic;
    
    @ApiModelProperty(value = "商品分类")
    @Field("category_id")
    private Long categoryId;
    
    @ApiModelProperty(value = "销量")
    @Field("sold_num")
    private Long soldNum;
    
    @ApiModelProperty(value = "该商品里面的分组信息")
    @Field("tag_list")
    private List<Long> tagList = new ArrayList<Long>(0) ;
    
    @ApiModelProperty(value = "该商品里面的评论数")
    @Field("praise_number")
    private Long praiseNumber  ;
    
    @ApiModelProperty(value = "该商品里面的好评论")
    @Field("positive_rating")
    private BigDecimal positiveRating  ;
}
