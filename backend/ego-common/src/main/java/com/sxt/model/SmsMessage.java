package com.sxt.model;

import java.io.Serializable;
import java.util.Map;

public class SmsMessage implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 电话名称
	 */
	private String  phoneNumbers ;
	/**
	 * 签名的名称
	 */
	private String  signName ;
	/**
	 * 模板id
	 */
	private String  templateCode ;
	/**
	 * 模板里面的数据
	 */
	private Map<String, String> templateParam ;
	public String getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(String phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public String getSignName() {
		return signName;
	}
	public void setSignName(String signName) {
		this.signName = signName;
	}
	public String getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}
	public Map<String, String> getTemplateParam() {
		return templateParam;
	}
	public void setTemplateParam(Map<String, String> templateParam) {
		this.templateParam = templateParam;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
