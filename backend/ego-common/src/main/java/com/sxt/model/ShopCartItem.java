package com.sxt.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 购物车里 的每个条目的信息
 * @author WHSXT-LTD
 *
 */
@Data
public class ShopCartItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("该商品是否被选择")
	private Boolean checked  = Boolean.TRUE;
	
	@ApiModelProperty("该商品的图片")
	private String pic ;
	
	@ApiModelProperty("购物车条目的id")
	private Long basketId ;
	
	@ApiModelProperty("该商品的id")
	private Long prodId ;
	
	@ApiModelProperty("该商品的名称")
	private String prodName ;
	@ApiModelProperty("sku的id")
	private Long skuId ;
	@ApiModelProperty("该商品的sku名称")
	private String skuName ;
	
	@ApiModelProperty("该商品的价格")
	private BigDecimal price ;
	
	@ApiModelProperty("该商品的数量")
	private Integer prodCount ;
}
