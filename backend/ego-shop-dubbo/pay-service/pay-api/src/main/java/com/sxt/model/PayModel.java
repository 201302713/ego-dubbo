package com.sxt.model;

import java.io.Serializable;

public class PayModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 支付类型
	 * 1 扫码支持
	 * 2 网页支付
	 */
	private Integer payType ;
	/**
	 * 订单编码，由商户自己提供，需要一个唯一的值
	 */
	private String outTradeNo;
	/**
	 * 订单的名称
	 */
	private String subject ;
	/**
	 * 订单的金额
	 */
	private String totalAmount ;
	/**
	 * 订单的折扣金额
	 */
	private String undiscountableAmount ;

	/**
	 * 商户的账号，一般商户没有开通子账号，则改号时定死的
	 */
	private String sellerId  ;
	/**
	 * 订单的描述
	 */
	private String body ;
	/**
	 * 操作员
	 */
	private String operatorId = "test_operator_id" ;

	/**
	 * 门店id
	 * 由支付宝生成，一般写个默认值就可以了
	 */
	private String storeId = "test_store_id" ;

	/**
	 * 支付的超时时间
	 */
	private String timeoutExpress = "30m" ;



	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getUndiscountableAmount() {
		return undiscountableAmount;
	}

	public void setUndiscountableAmount(String undiscountableAmount) {
		this.undiscountableAmount = undiscountableAmount;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getTimeoutExpress() {
		return timeoutExpress;
	}

	public void setTimeoutExpress(String timeoutExpress) {
		this.timeoutExpress = timeoutExpress;
	}



}
