package com.sxt.service;

import java.util.Map;

import com.sxt.model.PayModel;

/**
 * 支付接口
 * @author WHSXT-LTD
 *
 */
public interface EgoPayService {

	/**
	 * 使用支付的参数得到一个二维码地址或者表单
	 * @param model
	 * 支付参数
	 * @return
	 * 二维码地址 or 网页时它时form 表单而已
	 *
	 */
	String aliPay(PayModel model);
	
	String wechatPay(PayModel model);

	boolean checkRASSign(Map<String, String> params);
}
