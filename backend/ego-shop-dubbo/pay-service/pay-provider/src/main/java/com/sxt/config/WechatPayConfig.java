package com.sxt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;

@Configuration
public class WechatPayConfig {

	@Bean
	public WxPayService payService() {
		return new WxPayServiceImpl() ;
	}
}
