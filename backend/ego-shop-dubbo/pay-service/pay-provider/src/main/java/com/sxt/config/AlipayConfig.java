﻿package com.sxt.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016092000552404";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDZknmWOTWDDnP4Zpnktv9oeUcwUvrmMauSZL5xnCfmCIacOQ3upqSOGftas/YZhkI0474nNvAP1gKxgTb8GFDKP4l4W3MNx3C+De5rtGbCVEl89zeajj3cltSEctGejxQ6uMRSUecoMFZg5mmpPW6sQM3W8IPWw98tT2AGAEowXYbtV7CD5q+5+9BHRx7RIa+E3jKFQFN5rl4OR0fvLBQ99Mxxy09biE87qKi6mOIrJ0rOxj1wuCFJoU/HKNwk6m4+zDXftTCi0W3aLR1Kf6VFf/f3lVhXudf9rpDKnUO7EdE5mKoidhP9VVrcr5LGWL2lfWwsnXo0XE2KAVuZLEW/AgMBAAECggEBAKCJkMeEnPR/5JhD9Ierc5e97QY+JD1YJxXU2iTwKQx1/AUnoB0YvtDPYQUTRqKcyxtFdBhxU/8LVV9uTYkD1BD3gtLY/3MKVykVFHhKW4xGLxdTPqb/D6xFarf0FLHfNvkBPnLTVjH0QLgtK4eh+ZiGFJvlShJc4r8HE346FoPKEd5be4mBY6bTqrhhaBnC9rkgclcF1NAIV2JOF/JoKpfGZjXTKhyGVHbVbOYPyOsM17oNSHslNSzfPITHxeff+UOjTOGeWNmyHYdBq/rO0CxlioFR/OkNm7uFyh0YPh7Z3Yb3SmVPdTVMNBOQzd8/OGDjpcYX5lTER+TXJlzR1wECgYEA/KjfC7Xc1v8yyINGe6WA9OsBf/kLUJjP7HSYYaFZQYY5vNoJ+IczFPi14K91RgY02aDfn+vicGgcar1jS1vBaUnYxDvk41hu/PLVs6kMSgrOU1a6Q1wInW5gOUsqQnzxcHkK7wNVXLdQ9+MVeXcJ+/4kL6NClLMTXPgjp5ocyoECgYEA3HLZi4KnInUcGwT+6g+Z7cxlt/Ht+k7l051UBa27zLhOy1IZwlyqee0BvlpdaNSyefOy8YVrMM8deaECDlbVIBBgP3bUZvoEYc8CZUrZCvdowaEfiBmgfC7aRLMZu08ZVxuCly1/EpyKGBvrA8S2F1tmu4jiuKwYC+g7QGh+cD8CgYEA7UzqWIRRy7haiQsK+vzB9sZ2XSyaXPN5bftAV4oEW89mkSI0dTD+YDcME+CXg9I2dyRqhDg3MB4IepQwG0otiOcr6Xrbik+3xmmJHzmp6izmBk4RmUxqZvqQ857OLS5OUbXN+BioZvFh4cBtneyY+w2CDSAIW8Mpb4s7aaIMYoECgYB7thATVIzKr4HIP/N2Eq9FXdEvexsbvy8GpGHElgywBDZjr305L5uTtMentTp6W/KUsYgYNdFDoNdfrFVlCqGrzsTUmLD14RSYdv1XLrvChblKAEubqxX+2qQurXuhO/Byi8OrZyLKpQJyKSioArpVPTvXvifNjvYnqHmn2XwrLQKBgQDbN0f2YUZGoRO0pHY1jItosfXj8xHqWizcYtfJ95h8rvZlh+3gWlbl0k/L8xJFkp7j/fauus2w65Ek0YdZhhkQ4iu0SiYJHmgnh6IRoYYtFdgEVEa+Af2RWM9L/nn39QdvGz3fN/vZCwKH6tZveLzkKcdwkIrQXPWGWkQPbTxvrg==";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2nyeLlG7GyizB94gj8oHVSYB/6l8ZRZ9rCtxhIpn1IGbmVHSNoTpMFONyZTt5/aKTwzocTfk4MyzfWuCzYV0gdPJPEllNwELIo7G/e7KyJjXMSogqYIszQrpRFs7NlHlb7i0SckC6sLCLK0X275IMf+Px/xi8qW8fSBB4XEJNqi3cq4EvqRVidb/DDmrm30svHCQk3h/7X19FMBRCi8Rh43e/C8ZM/yOblT4MHdgFMCkbfFpSc0zOvd5f/9BaDeDqGrcgkfQAwkE879Zzcgi/szAd/B+Vu+GoWiM3o+gVpsdcllFE5PpK9ZmwvdeLTzSRty1voNCq7yGFLg2ykUJBQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

