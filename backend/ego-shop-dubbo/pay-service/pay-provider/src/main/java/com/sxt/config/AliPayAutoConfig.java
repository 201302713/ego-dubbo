package com.sxt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.service.AlipayTradeService;
import com.alipay.demo.trade.service.impl.AlipayTradeServiceImpl;

@Configuration
public class AliPayAutoConfig {

	@Bean
	public AlipayTradeService alipayTradeService() {
		   /** 一定要在创建AlipayTradeService之前调用Configs.init()设置默认参数
         *  Configs会读取classpath下的zfbinfo.properties文件配置信息，如果找不到该文件则确认该文件是否在classpath目录
         */
        Configs.init("zfbinfo.properties");

        /** 使用Configs提供的默认参数
         *  AlipayTradeService可以使用单例或者为静态成员对象，不需要反复new
         */
       return new AlipayTradeServiceImpl.ClientBuilder().build();
	}
	
	/**
	 * 往ioc 容器里面注入AlipayClient，为了集成网页支付
	 * @return
	 */
	@Bean
	public AlipayClient alipayClient() {
		return new DefaultAlipayClient(AlipayConfig.gatewayUrl, 
				 	AlipayConfig.app_id, 
				 	AlipayConfig.merchant_private_key, 
				 	"json", 
				 	AlipayConfig.charset, 
				 	AlipayConfig.alipay_public_key, 
				 	AlipayConfig.sign_type);
	}
}
