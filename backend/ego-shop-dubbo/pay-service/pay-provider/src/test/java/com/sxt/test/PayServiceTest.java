package com.sxt.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sxt.model.PayModel;
import com.sxt.service.EgoPayService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PayServiceTest {

	@Autowired
	private EgoPayService payService ;
	
	@Test
	public void testPay() {
		PayModel model = new PayModel();
		model.setSubject("买了表");
		model.setTotalAmount("2000000.00");
		model.setOutTradeNo("20123123123124");
		model.setBody("商品的描述");
		model.setPayType(2);
		String qrCode = payService.aliPay(model) ;
		if(qrCode!=null) {
			System.out.println(qrCode);
		}
	}
	
	@Test
	public void testWechatPay() {
		PayModel model = new PayModel();
		model.setSubject("买了表");
		model.setTotalAmount("2000000.00");
		model.setOutTradeNo("20123123123124");
		model.setBody("商品的描述");
		String wechatPay = payService.wechatPay(model);
		System.out.println(wechatPay);
	}
}
