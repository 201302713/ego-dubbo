package com.sxt;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
@MapperScan("com.sxt.mapper")
public class OrderApp {
   public static void main(String[] args) throws Exception {
	SpringApplication.run(OrderApp.class, args);
}

}
