package com.sxt.service.impl;

import com.sxt.entity.OrderRefund;
import com.sxt.mapper.OrderRefundMapper;
import com.sxt.service.OrderRefundService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class OrderRefundServiceImpl extends ServiceImpl<OrderRefundMapper, OrderRefund> implements OrderRefundService {

}
