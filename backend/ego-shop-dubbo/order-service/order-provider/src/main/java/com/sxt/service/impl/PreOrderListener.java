package com.sxt.service.impl;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.sxt.service.OrderService;
import com.sxt.vo.order.OrderVo;

@Component
public class PreOrderListener implements MessageListener {

	@Autowired
	private OrderService orderService ;
	
	@JmsListener(destination = "order.pre.queue")
	public void onMessage(Message message) {
		ObjectMessage objMessage =  (ObjectMessage) message ;
		try {
			OrderVo orderVo = (OrderVo)objMessage.getObject();
			orderService.createPreOrder(orderVo); // 完成创建
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}


}
