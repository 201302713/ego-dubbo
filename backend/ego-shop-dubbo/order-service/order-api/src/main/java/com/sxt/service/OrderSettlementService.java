package com.sxt.service;

import com.sxt.entity.OrderSettlement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderSettlementService extends IService<OrderSettlement> {

	/**
	 * 通过订单编号查询数据库里面的结算对象
	 * @param orderSn
	 * @return
	 */
	OrderSettlement queryOrderByOrderSn(String orderSn);

	/**
	 * 支付宝说支付成功了，我们把该订单的状态修改一下
	 * @param orderSn
	 */
	void updateOrderStatus(String orderSn);

}
