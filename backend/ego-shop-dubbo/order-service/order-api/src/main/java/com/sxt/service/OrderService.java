package com.sxt.service;

import com.sxt.entity.Order;
import com.sxt.entity.OrderSettlement;
import com.sxt.vo.order.OrderVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderService extends IService<Order> {

	/**
	 * 创建一个预订单，实现一个下单的流程
	 * @param orderVo
	 */
	void createPreOrder(OrderVo orderVo);

}
