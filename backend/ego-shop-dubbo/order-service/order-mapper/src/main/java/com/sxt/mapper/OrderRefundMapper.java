package com.sxt.mapper;

import com.sxt.entity.OrderRefund;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface OrderRefundMapper extends BaseMapper<OrderRefund> {

}
