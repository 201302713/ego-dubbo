package com.sxt.mapper;

import com.sxt.entity.Basket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface BasketMapper extends BaseMapper<Basket> {

}
