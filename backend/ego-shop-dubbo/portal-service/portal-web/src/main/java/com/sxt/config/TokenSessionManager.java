package com.sxt.config;

import java.io.Serializable;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import cn.hutool.core.lang.UUID;

@Configuration
public class TokenSessionManager extends DefaultWebSessionManager{

	private static final String AUTH_HEADER = "Authorization" ;
	
	@Override
	protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
		String token = WebUtils.toHttp(request).getHeader(AUTH_HEADER);
		if(StringUtils.hasText(token)) {
			return token ;
		}
		return UUID.randomUUID().toString() ;
	}
}
