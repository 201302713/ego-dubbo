package com.sxt.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.domain.ProdCommonData;
import com.sxt.entity.Prod;
import com.sxt.entity.ProdComm;
import com.sxt.entity.User;
import com.sxt.service.ProdCommService;
import com.sxt.service.ProdService;
import com.sxt.service.UserService;
import com.sxt.vo.ProdCommVo;

import cn.hutool.extra.emoji.EmojiUtil;
import ma.glasnost.orika.MapperFacade;

@RestController
public class ProdController {

	@Reference(check = false)
	private ProdService prodService ;

	@Reference(check = false)
	private ProdCommService prodCommService ;
	
	@Autowired
	private MapperFacade mapperFacade ;
	
	@Autowired
	private UserService userService ;

	@GetMapping("/prod/prodInfo")
	public ResponseEntity<Prod> prodInfo(Long prodId){
		Prod prod = prodService.getById(prodId) ;
		return ResponseEntity.ok(prod) ;
	}

	/**
	 * 评论的预览
	 * @param prodId
	 * @return
	 */
	@GetMapping("/prodComm/prodCommData")
	public ResponseEntity<ProdCommonData> loadProdComm(Long prodId){
		ProdCommonData prodCommData = prodCommService.loadProdCommData(prodId);
		return ResponseEntity.ok(prodCommData) ;
	}

	/**
	 * 评论数据的分页查询
	 * @param prodId
	 * @param current
	 * @param size
	 * @param evaluate
	 * @return
	 */
	@GetMapping("prodComm/prodCommPageByProd")
	public ResponseEntity<IPage<ProdCommVo>> findProdCommByPage(
			Long prodId ,
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size ,
			@RequestParam(defaultValue = "-1")Integer evaluate // -1 代表查询所有的评论数据
			){
		Page<ProdComm> page = new Page<ProdComm>(current,size) ;
		ProdComm prodComm = new ProdComm();
		prodComm.setEvaluate(evaluate) ;// -1(所有的) 0（好评） 1（中评） 2（差评） 3 （有图）
		IPage<ProdComm> pageData = prodCommService.findByPage(page, prodComm); // 但是里面没有用户的头像和昵称
		List<ProdComm> records = pageData.getRecords();
		Page<ProdCommVo> pageResult = new Page<ProdCommVo>(current, size);
		if(records==null || records.isEmpty()) { // 不要给前端之间给null
			return ResponseEntity.ok(pageResult);
		}
		pageResult.setTotal(pageData.getTotal()) ;
		List<ProdCommVo> prodCommVoRec = new ArrayList<ProdCommVo>(records.size());
		for (ProdComm prodComm2 : records) {
			ProdCommVo prodCommVo = mapperFacade.map(prodComm2, ProdCommVo.class);
			String userId = prodComm2.getUserId();
			User user = userService.getById(userId);
			String nickName = user.getNickName(); // 用户的昵称可能包含表情
			prodCommVo.setNickName(EmojiUtil.toUnicode(nickName));
			prodCommVo.setPic(user.getPic());
			prodCommVoRec.add(prodCommVo) ;
		}
		pageResult.setRecords(prodCommVoRec) ;
		
		return ResponseEntity.ok(pageResult) ;
	}
}
