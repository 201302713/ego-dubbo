package com.sxt.controller;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.Category;
import com.sxt.entity.Prod;
import com.sxt.model.ProdSolrDto;
import com.sxt.service.CategoryService;
import com.sxt.service.SearchService;

import io.swagger.annotations.ApiOperation;

/**
 *  主要完成商品的搜索
 * @author WHSXT-LTD
 *
 */
@RestController
public class SearchController {

	@Reference(check = false)
	private SearchService searchService ;
	
	@Reference(check = false)
	private CategoryService categoryService ;

	/**
	 * 根据活动的id 查询商品
	 * /prod/prodListByTagId?tagId=3&size=6
	 */
	@GetMapping("/prod/prodListByTagId")
	public ResponseEntity<IPage<ProdSolrDto>> findByTagId(
			Long tagId ,
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size){
		Page<ProdSolrDto> page = new Page<ProdSolrDto>(current, size) ;
		Page<ProdSolrDto> pageData = searchService.searchForTag(page, tagId) ;
		return ResponseEntity.ok(pageData);
	}


	/**
	 * 
	 * 关键字查询
	 * /search/searchProdPage?current=1&prodName=xxx&size=10&sort=0
	 */
	@GetMapping("/search/searchProdPage")
	public ResponseEntity<IPage<ProdSolrDto>> findByCatId(
			String prodName ,
			Integer sort,
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size){
		Page<ProdSolrDto> page = new Page<ProdSolrDto>(current, size) ;
		Page<ProdSolrDto> pageData = searchService.searchForKeyword(page, prodName, sort);
		return ResponseEntity.ok(pageData);
	}
	
	
	@ApiOperation("直接查询所有的二级菜单")
	@GetMapping("/category/categoryInfo")
	public ResponseEntity<List<Category>> getSubCateList(){
		List<Category> cateGories = categoryService.getSubCateList();
		return ResponseEntity.ok(cateGories) ;
	}
	
	/**
	 * 根据分类的id
	 * /prod/pageProd?categoryId=96
	 */
	@GetMapping("/prod/pageProd")
	public ResponseEntity<IPage<ProdSolrDto>> findByCatId(
			Long categoryId ,
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size){
		Page<ProdSolrDto> page = new Page<ProdSolrDto>(current, size) ;
		Page<ProdSolrDto> pageData = searchService.searchForCategory(page, categoryId) ;
		return ResponseEntity.ok(pageData);
	}
	
	
}
