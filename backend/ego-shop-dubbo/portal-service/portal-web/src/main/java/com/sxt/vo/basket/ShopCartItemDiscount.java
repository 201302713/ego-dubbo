package com.sxt.vo.basket;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.sxt.model.ShopCartItem;

import lombok.Data;

@Data
public class ShopCartItemDiscount implements Serializable {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 为了优惠价准备的，现在还使用不到
	 */
	 private List<ShopCartItem> shopCartItems  = Collections.emptyList();
} 
