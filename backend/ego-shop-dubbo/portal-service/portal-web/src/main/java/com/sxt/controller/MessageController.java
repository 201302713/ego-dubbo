package com.sxt.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.model.SmsMessage;
import com.sxt.model.WechatMsg;

@RestController
public class MessageController {

	@Autowired
	private JmsTemplate jmsTemplate ;
	
	@GetMapping("/p/sms/send")
	public ResponseEntity<Void> sendSmsValidateCode(@RequestParam(required = true)String phonenum){
		
		// 构造短信的对象
		SmsMessage smsMessage = new SmsMessage();
		smsMessage.setPhoneNumbers(phonenum);
		smsMessage.setTemplateCode("SMS_181852339");
		smsMessage.setSignName("SXT教育");
		Map<String, String> templateParam = new HashMap<String,String>();
		templateParam.put("code", "8888");
		smsMessage.setTemplateParam(templateParam);
		//发送到mq 里面
		jmsTemplate.convertAndSend("sms.msg.queue", smsMessage);
		return ResponseEntity.ok().build() ;
	}
	
	@GetMapping("/p/wechat/send")
	public ResponseEntity<Void> sendValidateCode(@RequestParam(required = true)String phonenum){
		 // 即将要发送一个微信的消息了
		// 只需要把消息放在队列里面就可以了
		WechatMsg weChatMsg = new WechatMsg();
		weChatMsg.setToUser(phonenum);
		weChatMsg.setTemplateId("WNWLKl8tveMKIYmQr3tvttqiKtRWxObI5fXdn5U9R5c");
		weChatMsg.setTopColor("#FF6600");
		weChatMsg.setUrl("www.whsxt.com");
  //尊敬的{{user.DATA}}： 感谢您绑定您的账号信息，以下是您的验证码信息！ 验证码：{{code.DATA}} 有效期：{{time.DATA}} 感谢您对EGO商城的支持!		
		HashMap<String, Map<String, String>> data = new HashMap<String, Map<String,String>>();
		data.put("user", WechatMsg.builde("梁天东", "#0099FF"));
		data.put("code",  WechatMsg.builde("8888", "#0099FF"));
		data.put("time",  WechatMsg.builde("1 分钟", "#0099FF"));
		weChatMsg.setData(data);
		jmsTemplate.convertAndSend("wechat.msg.queue", weChatMsg);
		return ResponseEntity.ok().build();
	}
}
