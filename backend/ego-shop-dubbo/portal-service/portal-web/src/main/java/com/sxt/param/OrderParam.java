package com.sxt.param;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 输入的订单参数
 * @author WHSXT-LTD
 *
 */
@Data
public class OrderParam {
    
	@ApiModelProperty("地址的id")
	private Long addrId;
	
	@ApiModelProperty("所选的购物车的条目id")
    private List<Long> basketIds ;
	
	@ApiModelProperty("优惠劵的id")
    private List<Long> couponIds ;
	
	@ApiModelProperty("是否使用优惠劵")
    private Integer userChangeCoupon ;
}
