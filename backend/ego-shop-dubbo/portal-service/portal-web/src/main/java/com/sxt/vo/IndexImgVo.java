package com.sxt.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class IndexImgVo {
	
	@ApiModelProperty(value = "主键")
	private Long imgId;
	
	@ApiModelProperty(value = "图片")
	private String imgUrl;
	
	@ApiModelProperty(value = "关联")
	private Long relation;
	
}
