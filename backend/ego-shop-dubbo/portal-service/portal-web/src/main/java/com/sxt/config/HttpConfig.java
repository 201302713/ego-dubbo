package com.sxt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HttpConfig {

	/**
	 * 在java 里面怎么发送Http 请求:
	 * 1 HttpURLConnection (下载图片和电子书)
	 * 2 HttpClient(发送Http 请求) apach 的
	 * 这2 个不好用,spring 给我们封装了一个RestTemplate 使用他发请求非常简单
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}

//	public static void main(String[] args) {
//		RestTemplate restTemplate = new RestTemplate() ;
//		/**
//		 * 请求三要素:
//		 * 1  url
//		 * 2 method
//		 * 3 data
//		 * 
//		 * Entity: 代表响应体 ,包含响应码,响应状态,以及响应的值
//		 * Object: 简化的,只要响应的值
//		 * ResponseType: 响应的类型,http 是超文件协议, html xml(对象) json(对象)
//		 * // get 请求放在url 里面
//		 */
//		ResponseEntity<String> entity = restTemplate.getForEntity("https://www.baidu.com/", String.class);
//		System.out.println("响应码:"+entity.getStatusCodeValue()+"响应状态:"+entity.getStatusCode());
//		System.out.println(entity.getBody()); // 响应的数据
//		String body = restTemplate.getForObject("https://www.baidu.com/", String.class);
//		System.out.println(body);
//		/**
//		 * 发post 请求时,参数会被自动转化为json
//		 */
//		restTemplate.postForObject(url, object, responseType)
//		
//		/**
//		 * 删除
//		 */
//		restTemplate.delete("xxx");
//		
//		/**
//		 * 发put 请求时,参数会被自动转化为json
//		 */
//		restTemplate.put(url, request);
//	}
//}
