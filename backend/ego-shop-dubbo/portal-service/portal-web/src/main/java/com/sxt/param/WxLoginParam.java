package com.sxt.param;

import lombok.Data;

@Data
public class WxLoginParam {
	/**
	 * 微信的验证码
	 */
	private String principal ;
}
