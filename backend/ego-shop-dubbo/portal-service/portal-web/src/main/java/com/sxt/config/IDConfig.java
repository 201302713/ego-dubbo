package com.sxt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.hutool.core.lang.Snowflake;

@Configuration
public class IDConfig {

	@Bean
	public Snowflake snowflake() {
		return new Snowflake(1, 1) ;
	}
}
