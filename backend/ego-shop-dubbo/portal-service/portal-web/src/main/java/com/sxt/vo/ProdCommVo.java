package com.sxt.vo;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProdCommVo {
	
	@ApiModelProperty("评论时间")
	private LocalDateTime recTime ;
	
	@ApiModelProperty("评论用户的头像")
	private String pic;
	
	@ApiModelProperty("评论用户的昵称")
	private String nickName;
	
	@ApiModelProperty("评论用户的分数")
	private Integer score ;
	
	@ApiModelProperty("评论内容")
	private String content ;
	
	@ApiModelProperty("店铺的回复")
	private String replyContent ;
	
	@ApiModelProperty("有图评论的图片")
	private String pics ;
}
