package com.sxt.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NoticeVo {
	@ApiModelProperty(value = "公告id")
	private Long id;
	@ApiModelProperty(value = "公告标题")
	private String title;

}
