package com.sxt.controller;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.entity.IndexImg;
import com.sxt.entity.Notice;
import com.sxt.entity.ProdTag;
import com.sxt.service.IndexImgService;
import com.sxt.service.NoticeService;
import com.sxt.service.ProdTagService;
import com.sxt.vo.IndexImgVo;
import com.sxt.vo.NoticeVo;
import com.sxt.vo.ProdTagVo;

import ma.glasnost.orika.MapperFacade;

/**
 * 完成首页数据的加载
 * @author WHSXT-LTD
 *
 */
@RestController
public class IndexController {

	@Reference(check = false)
	private IndexImgService indexImgService ;
	
	@Autowired
	private MapperFacade mapperFacade ;
	
	@Reference(check = false)
	private NoticeService noticeService ;
	
	@Reference(check = false)
	private ProdTagService  prodTagService ;
	/**
	 * 需要在protal-web 远程调用store 服务
	 * @return
	 */
	@GetMapping("/indexImgs")
	public ResponseEntity<List<IndexImgVo>> loadIndexImgs(){
		// 为什么不在service 查询时,不查询这些字段,非常在web 层里面做对象映射来减少字段?
		//1 indexImgService 服务提供者,服务提供者的作用:提供统一的,完整的服务
		// 因为服务提供者给很多人提供服务,服务提供者若不提供完整的统一的服务器,那服务提供者要考虑的情况将非常多
		// 而且服务器提供者和服务消费者之间的交互,一般在局域网内部,它的网速,不需要流量费用
		List<IndexImg> indexImgs = indexImgService.loadIndexImgs();
		// 怎么把entity->vo 对象IndexImg->IndexImgVo
		// BeanUtils.copyProperties(source, target); 按照属性的名称对应来做属性的copy ,性能较高,被你new 对象set 属性,2倍左右
		// mapperFacade: map()entity->vo  mapAsList()entityList->voList mapAsArray->entityArray->voArray
		List<IndexImgVo> indexImgVos = mapperFacade.mapAsList(indexImgs, IndexImgVo.class);
		return ResponseEntity.ok(indexImgVos) ;
	}
	
	/**
	 * 加载在首页里面显示的置顶通知的title
	 * 
	 */
	@GetMapping("/shop/notice/topNoticeList")
	public ResponseEntity<List<NoticeVo>> getIndexTopNotic(){
		List<Notice> notices = noticeService.listAllViewNotice(); // 包含所有的通知
		if(notices==null || notices.isEmpty()) {
			return ResponseEntity.ok(Collections.emptyList()) ;
		}
		Iterator<Notice> iterator = notices.iterator();// 我只要置顶的通知
		while (iterator.hasNext()) { // 移除不是置顶通知的对象
			Notice next = iterator.next();
			if(!next.getIsTop().equals(1)) {
				iterator.remove(); // 在迭代器里面remove ,在集合里面不允许边循环边删除,因为会改变集合的长度,迭代器里面可以
			}
		}
		List<NoticeVo> noticeVos = mapperFacade.mapAsList(notices, NoticeVo.class);
		return ResponseEntity.ok(noticeVos);
	}
	
	@GetMapping("/shop/notice/noticeList")
	public ResponseEntity<List<Notice>> getAllNotice(){
		List<Notice> notices = noticeService.listAllViewNotice();
		return ResponseEntity.ok(notices);
	}
	
	@GetMapping("/prod/tag/prodTagList")
	public ResponseEntity<List<ProdTagVo>> getAllProdTag(){
		List<ProdTag> prodTags = prodTagService.listAllProdTag();
		List<ProdTagVo> prodTagVos = mapperFacade.mapAsList(prodTags, ProdTagVo.class);
		return ResponseEntity.ok(prodTagVos);
	}
	
}
