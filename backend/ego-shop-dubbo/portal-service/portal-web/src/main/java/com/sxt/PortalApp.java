package com.sxt;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.sxt.mapper")
@EnableDubbo
public class PortalApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(PortalApp.class, args);
	}

}
