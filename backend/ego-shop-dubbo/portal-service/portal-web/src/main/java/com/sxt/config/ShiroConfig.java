package com.sxt.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.IRedisManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShiroConfig {

	/**
	 * SecurityManager
	 * DefaultWebSecurityManager
	 */
	@Bean
	public DefaultWebSecurityManager defaultWebSecurityManager(SessionDAO sessionDao,UserRealm realm,TokenSessionManager sessionManager) {
		DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
		defaultWebSecurityManager.setRealm(realm);
		sessionManager.setSessionDAO(sessionDao);
		defaultWebSecurityManager.setSessionManager(sessionManager);
		return defaultWebSecurityManager ;
	}
	
	/**
	 * session 存储在redis 里面
	 */
	@Bean
	public SessionDAO redisSessionDao(IRedisManager redisManager) {
		RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
		redisSessionDAO.setRedisManager(redisManager);
		return redisSessionDAO ;
	}
	/**
	 * 得到一个可以操作的redis 对象
	 * @return
	 */
	@Bean
	public IRedisManager redisManager() {
		RedisManager redisManager = new RedisManager();
		redisManager.setHost("www.yanli.ltd:6379");
		return redisManager ;
	}
	
	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager defaultWebSecurityManager) {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setLoginUrl("/login");// 登录页面->Get
		shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
		shiroFilterFactoryBean.setUnauthorizedUrl("/login");//用户没有权限时,跳转该页面登录页面->Get
		Map<String, String> filterChainDefinitionMap = new HashMap<String, String>();
		filterChainDefinitionMap.put("/login", "anon");
		filterChainDefinitionMap.put("/p/order/notify/**", "anon");
		filterChainDefinitionMap.put("/**", "authc");
		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap );
		return shiroFilterFactoryBean;
	}
	
	/**
	 * 在shiro 里面还有基于注解的权限验证
	 */
//	@Bean
//	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager defaultWebSecurityManager) {
//		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
//		authorizationAttributeSourceAdvisor.setSecurityManager(defaultWebSecurityManager);
//		return authorizationAttributeSourceAdvisor ;
//	}
//	
//	@Bean
//	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
//		DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
//		// false 为jdk true 为cglib
//		defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
//		return defaultAdvisorAutoProxyCreator;
//	}
}
