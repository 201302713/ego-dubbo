package com.sxt.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MoneyResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("最终金额")
	private BigDecimal finalMoney = new BigDecimal("9999.99");
	
	@ApiModelProperty("总金额")
	private BigDecimal totalMoney = new BigDecimal("9999.99");
	
	@ApiModelProperty("优惠金额")
	private BigDecimal subtractMone = new BigDecimal("0.00");
	
	
	@ApiModelProperty("所选商品的总数量")
	private Integer total ;

}
