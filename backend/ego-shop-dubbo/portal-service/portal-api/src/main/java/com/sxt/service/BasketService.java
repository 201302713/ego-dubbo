package com.sxt.service;

import com.sxt.entity.Basket;
import com.sxt.model.MoneyResult;
import com.sxt.model.ShopCartItem;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface BasketService extends IService<Basket> {

	/**
	 *  添加到购物车里面
	 * @param basket
	 */
	void addCart(Basket basket);

	/**
	 * 获取用户购物车的总条数
	 * @param userId
	 * @return
	 */
	Integer getTotalCount(String userId);

	/**
	 * 得到一个用户购物车条目的数据
	 * @param userId
	 * @return
	 */
	List<ShopCartItem> getCartInfo(String userId);

	/**
	 * 计算购物的总金额
	 * @param basketIds
	 * @return
	 */
	MoneyResult getTotalMoney(List<Long> basketIds);

	/**
	 * 在用户点击去结算时，先通过购物车的条目的id 查询购物车的条目
	 * @param basketIds
	 * @return
	 */
	List<ShopCartItem> getOrderConfirm(List<Long> basketIds);

	/**
	 * 清空用户的一些商品数据
	 * @param cartItems
	 */
	void clearCart(List<ShopCartItem> cartItems);

}
