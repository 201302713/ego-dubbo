package com.sxt.service;

import com.sxt.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface UserService extends IService<User> {

	/**
	 * 通过用户查询用户
	 * @param openId
	 * @return
	 */
	User findUserByUsername(String openId);



}
