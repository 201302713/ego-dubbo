package com.sxt.service.impl;

import com.sxt.entity.User;
import com.sxt.mapper.UserMapper;
import com.sxt.service.UserService;

import cn.hutool.extra.emoji.EmojiUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	/**
	 * sql 注入:
	 * select * from user where username = ${username} admin or '1=1' // 有风险
	 * select * from user where username = #{username} 没有sql 注入
	 */
	@Override
	public User findUserByUsername(String openId) {
		log.info("查询用户openid为{}用户",openId);
		User user = userMapper.selectById(openId); //user_id
		return user;
	}
	
	@Override
	public boolean updateById(User entity) {
		Assert.notNull(entity,"修改的用户不能为空");
		log.info("修改用户{}",JSONUtil.toJsonStr(entity));
		// 处理性别:
		String sex = entity.getSex(); //
		try {
			Integer sexTag = Integer.valueOf(sex);
			sex = sexTag.equals(1)?"M":sexTag.equals(2)?"F":"UNKNOW";
			entity.setSex(sex);
		}catch (Exception e) {
			System.out.println("不是一个数据");
		}
		// 处理昵称 // 表情时字符串可以存储,但是数据库无法存储,需要将标志编码为能存储的字符串
		//EmojiUtil 可以处理,hutool 里面有
		String nickName = entity.getNickName();
		String alias = EmojiUtil.toAlias(nickName); // 数据库能存储
		entity.setNickName(alias);
		return super.updateById(entity);
	}
	
	public static void main(String[] args) {
		String nickName = "天🙏东😂"; // 
		String alias = EmojiUtil.toAlias(nickName);
		System.out.println(alias);
		String emoji = EmojiUtil.toUnicode(alias);
		System.out.println(emoji);
		
	}

}
