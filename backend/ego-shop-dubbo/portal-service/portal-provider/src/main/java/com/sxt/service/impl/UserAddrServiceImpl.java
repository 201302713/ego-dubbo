package com.sxt.service.impl;

import com.sxt.entity.UserAddr;
import com.sxt.mapper.UserAddrMapper;
import com.sxt.service.UserAddrService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户配送地址 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class UserAddrServiceImpl extends ServiceImpl<UserAddrMapper, UserAddr> implements UserAddrService {

	@Autowired
	private UserAddrMapper userAddrMapper ;
	
	@Override
	public UserAddr getUserDefaultAddr(String userId) {
		log.info("查询用户{}的默认收货地址",userId);
		return userAddrMapper.selectOne(new LambdaQueryWrapper<UserAddr>()
					.eq(UserAddr::getUserId, userId)
					.eq(UserAddr::getCommonAddr,1));
	}

}
