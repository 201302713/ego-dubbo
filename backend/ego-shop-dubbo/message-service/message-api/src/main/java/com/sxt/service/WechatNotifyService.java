package com.sxt.service;

import com.sxt.model.WechatMsg;

/**
 * 使用微信给用户发通知消息
 * @author WHSXT-LTD
 *
 */
public interface WechatNotifyService {

	/**
	 * 给用户发送消息
	 * @param message
	 */
	void sendMsg(WechatMsg message);
}
