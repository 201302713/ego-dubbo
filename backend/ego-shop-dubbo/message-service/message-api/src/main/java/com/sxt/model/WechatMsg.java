package com.sxt.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WechatMsg  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 发送给那个用户
	 * 就是 商户发送给我们那个粉丝，或者会员
	 * 以会员的openId为主
	 * 只要会员关注了我们，我们才能才能给别人
	 * 因为会员关注我们后，我们会得到该会员的一个Openid
	 */
	@JsonProperty("touser")
	private String toUser ;
	
	/**
	 * 消息的模板
	 * 消息并不是你瞎写的，需要看模板，而模板是需要申请的
	 * 1 测试账号时，模板可以自己随便定义
	 * 2 正式环境时，你必须使用系统定义好的模板
	 */
	@JsonProperty("template_id")
	private String templateId;
	/**
	 * 别人点击你的这个消息，会跳转到那个url 里面
	 */
	private String url ;
	/**
	 * 顶部的颜色
	 */
	@JsonProperty("topcolor")
	private String topColor ;
	/**
	 * 模板里面的数据
	 */
	private Map<String,Map<String,String>> data ;
	
	public static Map<String,String> builde(String value,String color){
		Map<String, String> data = new HashMap<String, String>();
		data.put("value", value) ;
		data.put("color", color) ;
		return data ;
	}
	
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTopColor() {
		return topColor;
	}
	public void setTopColor(String topColor) {
		this.topColor = topColor;
	}
	public Map<String, Map<String, String>> getData() {
		return data;
	}
	public void setData(Map<String, Map<String, String>> data) {
		this.data = data;
	}

   

}
