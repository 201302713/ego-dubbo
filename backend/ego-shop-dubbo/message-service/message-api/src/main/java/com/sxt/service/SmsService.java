package com.sxt.service;

import com.sxt.model.SmsMessage;

/**
 * 短信服务
 * @author WHSXT-LTD
 *
 */
public interface SmsService {

	/**
	 * 发送短信消息
	 * @param message
	 */
	void sendSms(SmsMessage message) ;
}
