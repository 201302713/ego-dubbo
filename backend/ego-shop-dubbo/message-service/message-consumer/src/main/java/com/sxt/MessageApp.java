package com.sxt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessageApp {
  
	public static void main(String[] args) throws Exception {
		SpringApplication.run(MessageApp.class, args);
	}

}
