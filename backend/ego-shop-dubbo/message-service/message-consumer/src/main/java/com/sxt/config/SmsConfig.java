package com.sxt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;

@Configuration
public class SmsConfig {

	@Value("${aliyun.id}") // 
	private String accessKeyId ;
	
	@Value("${aliyun.secret}")
	private String accessSecret ; //
	@Bean
	public IAcsClient client() {
		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
		IAcsClient client = new DefaultAcsClient(profile);
		return client ;
	}

}
