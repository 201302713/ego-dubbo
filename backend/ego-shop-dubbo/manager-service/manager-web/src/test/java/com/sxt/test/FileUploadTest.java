package com.sxt.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;

import javax.imageio.stream.FileImageInputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tobato.fastdfs.domain.fdfs.MetaData;
import com.github.tobato.fastdfs.domain.fdfs.StorageNode;
import com.github.tobato.fastdfs.domain.fdfs.StorageNodeInfo;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadFileWriter;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.github.tobato.fastdfs.service.TrackerClient;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FileUploadTest {

	@Autowired
	private FastFileStorageClient  fastFileStorageClient ;
	
	@Autowired
	private TrackerClient trackerClient ;
	
	/**
	 * 文件上传
	 */
	@Test
	public void testFileUpload() {
		File file = new File("D:/4.mp4");
		try {
			StorePath uploadFile = fastFileStorageClient.uploadFile(new FileInputStream(file), file.length(), "mp4", new HashSet<MetaData>());
			// StorePath [group=g1, path=M00/00/00/rBOsS14AYySACNI5AADbCKb1gk0863.jpg]
			System.out.println(uploadFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 文件的下载
	 */
	@Test
	public void testdownloadFile() {
		// 下载的数据是字节，但是我们直接需要的是文件  字节->文件
		String path = "D://3.jpg";
		fastFileStorageClient.downloadFile("g1", "M00/00/00/rBOsS14AYySACNI5AADbCKb1gk0863.jpg", new DownloadFileWriter(path));
	}
	
	/**
	 * 文件的删除
	 */
	@Test
	public void delete() {
		fastFileStorageClient.deleteFile("g1", "M00/00/00/rBOsS14AYySACNI5AADbCKb1gk0863.jpg");
		System.err.println("ok");
	}
	
	@Test
	public void trackerTest() {
		/**
		 * 给tracker 一个组名和文件名称，得到该文件到底放在那个storage 上面
		 * 该功能在下载时使用
		 */
//		StorageNodeInfo fetchStorage = trackerClient.getFetchStorage("g1", "M00/00/00/rBOsS14AahqAaGZUAADbCKb1gk0327.jpg");
//		System.out.println(fetchStorage);
		/**
		 * 使用tracker 得到当前注册到tracker 上面storage
		 * 该功能，在文件上传时使用
		 */
		while (true) {
			StorageNode storeStorage = trackerClient.getStoreStorage(); 
			System.out.println(storeStorage);
		}
		
	}
	
}
