package com.sxt.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginParam {
	
	@ApiModelProperty(" 用户名称")
	private String principal ; // 用户名称
	
	@ApiModelProperty("用户的密码")
	private String credentials ; // 用户的密码
	
	
	@ApiModelProperty("在用户获取验证码时的uuid值")
	private String sessionUUID ; // 在用户获取验证码时的uuid值
	
	@ApiModelProperty("用户之前获取的验证码")
	private String imageCode; // 用户之前获取的验证码
}
