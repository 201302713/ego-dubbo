package com.sxt.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.github.tobato.fastdfs.domain.fdfs.MetaData;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;

import io.swagger.annotations.ApiOperation;

/**
 * 文件上传
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/admin/file")
public class FileController {

	@Autowired
	private FastFileStorageClient fastFileStorageClient ;
	
	private String storagePath = "http://www.yanli.ltd:8888/" ;
	
	/**
	 * 文件上传成功后，给一个能访问的文件的地址就可以了
	 * @param file
	 * @return
	 */
	@ApiOperation("VUE的文件上传")
	@PostMapping("/upload/element")
	public ResponseEntity<String> uploadFile(@RequestParam(name = "file")MultipartFile file){
	
		String name = file.getOriginalFilename(); // 1.jpg
		try {
			StorePath uploadFile = fastFileStorageClient.uploadFile(
					  file.getInputStream(), 
					  file.getSize(), 
					  name.substring(name.lastIndexOf(".")+1), 
					  new HashSet<MetaData>(0));
			return ResponseEntity.ok(storagePath+uploadFile.getFullPath());
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("上传文件失败");
		}
	}
}
