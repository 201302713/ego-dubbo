package com.sxt.controller.item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.ProdComm;
import com.sxt.service.ProdCommService;

import io.swagger.annotations.ApiOperation;

/**
 * 商品评论数据的数据接口
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/prod/prodComm")
public class ProdCommController {

	@Reference(check = false)
	private ProdCommService prodCommService ;
	
	
	@GetMapping("/page")
	@ApiOperation("商品评论数据的分页查询")
	public ResponseEntity<IPage<ProdComm>> findByPage(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size ,
			String prodName,
			Integer status
			){
		Page<ProdComm> page = new Page<ProdComm>(current, size);
		ProdComm prodComm = new ProdComm();
		prodComm.setProdName(prodName);
		prodComm.setStatus(status);
		IPage<ProdComm> pageData = prodCommService.findByPage(page,prodComm);
		List<ProdComm> records = pageData.getRecords();
		for (ProdComm prodComm2 : records) {
			Map<String, Object> user = new HashMap<String, Object>();
			String userId = prodComm2.getUserId(); // 通过用户的id 查询用户的昵称
			user.put("userId", userId);
			user.put("nickName", "老雷") ;
			prodComm2.setUser(user);
		}
		return ResponseEntity.ok(pageData);
	}
	
	@GetMapping("/info/{id}")
	public ResponseEntity<ProdComm> findById(@PathVariable("id") Long id){
		ProdComm prodComm = prodCommService.getById(id);
		return ResponseEntity.ok(prodComm);
	}
	
	@PutMapping
	@ApiOperation("掌柜的回复")
	public ResponseEntity<Void> update(@RequestBody ProdComm prodComm){
		prodCommService.updateById(prodComm) ;
		return ResponseEntity.ok().build();
	}
 }
