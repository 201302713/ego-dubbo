package com.sxt.controller.item;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.ProdTag;
import com.sxt.service.ProdTagService;

import io.swagger.annotations.ApiOperation;

/**
 * 商品分组的数据接口
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/prod/prodTag")
public class ProdTagController {

	@Reference(check = false)
	private ProdTagService prodTagService ;

	@ApiOperation("商品分组的分页查询")
	@GetMapping("/page")
	public ResponseEntity<IPage<ProdTag>> findByPage(
			@RequestParam (defaultValue = "1")Integer current,
			@RequestParam (defaultValue = "10")Integer  size,
			String title,
			Integer status
			){
		ProdTag prodTag = new ProdTag();
		prodTag.setTitle(title);
		prodTag.setStatus(status);
		IPage<ProdTag> pageData = prodTagService.findByPage(new Page<ProdTag>(current, size),prodTag);
		return ResponseEntity.ok(pageData);
	}
	
	@ApiOperation("新增一个分组")
	@PostMapping
	public ResponseEntity<Void> save(@RequestBody ProdTag prodTage){
		prodTagService.save(prodTage);
		return ResponseEntity.ok().build() ; 
	}
	
	@ApiOperation("全查询所有启用商品的分组信息")
	@GetMapping("/listTagList")
	public ResponseEntity<List<ProdTag>> list(){
		List<ProdTag> list = prodTagService.list();
		return ResponseEntity.ok(list) ;
	}
}
