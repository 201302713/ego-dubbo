package com.sxt.controller.item;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.entity.Category;
import com.sxt.service.CategoryService;

import io.swagger.annotations.ApiOperation;

/**
 * 商品分类的数据接口
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/prod/category")
public class CategoryController {

	@Reference(check = false)
	private CategoryService categoryService ;

	@GetMapping("/table")
	@ApiOperation("菜单的全查询")
	public ResponseEntity<List<Category>> list(){
		List<Category> list = categoryService.list();
		return ResponseEntity.ok(list) ;
	}

	@ApiOperation("只查询父菜单")
	@GetMapping("/listCategory")
	public ResponseEntity<List<Category>> listParentCategory(){
		List<Category> list = categoryService.listParentCategory();
		return ResponseEntity.ok(list) ;
	}

	@PostMapping
	@ApiOperation("新增一个商品的分类")
	public ResponseEntity<Void> save(@RequestBody Category category){
		categoryService.save(category); // 新增时需要处理grade 的值
		return ResponseEntity.ok().build();
	}
}
