package com.sxt.controller.item;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.ProdProp;
import com.sxt.entity.ProdPropValue;
import com.sxt.service.ProdPropService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/prod/spec")
public class ProdPropController {

	@Reference(check = false)
	private ProdPropService prodPropService ;
	/**
	 * 分页查询
	 */
	@GetMapping("/page")
	@ApiOperation("完成商品规格的分页查询")
	public ResponseEntity<IPage<ProdProp>> page(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size,
			String propName
			){
		Page<ProdProp> page = new Page<ProdProp>(current, size);
		ProdProp prodProp = new ProdProp();
		prodProp.setPropName(propName);
		IPage<ProdProp> pageData = prodPropService.findByPage(page,prodProp);
		return ResponseEntity.ok(pageData);
	}


	/**
	 * 新增一个商品的属性
	 * @param prodProp
	 * @return
	 */
	@PostMapping
	@ApiOperation("新增商品的属性和值")
	public ResponseEntity<Void> add(@RequestBody ProdProp prodProp){
		prodPropService.save(prodProp);
		return ResponseEntity.ok().build();
	}

	/**
	 * 修改属性和属性的值
	 */
	@PutMapping
	@ApiOperation("修改属性和属性的值")
	public ResponseEntity<Void> update(@RequestBody ProdProp prodProp){
		prodPropService.updateById(prodProp) ;
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/{id}")
	@ApiOperation("删除商品的属性和值")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id){
		prodPropService.removeById(id);
		return ResponseEntity.ok().build() ;
	}

	@ApiOperation("查询商品的规格列表")
	@GetMapping("/list")
	public ResponseEntity<List<ProdProp>>  list(){
		List<ProdProp> list = prodPropService.list();
		return ResponseEntity.ok(list);
	}

	@ApiOperation("根据属性的名称查询属性的值")
	@GetMapping("/listSpecValue/{id}")
	public ResponseEntity<List<ProdPropValue>> listValue(@PathVariable("id")Long id){
		List<ProdPropValue> list = prodPropService.listPropValues(id);
		return ResponseEntity.ok(list);
	}
}
