package com.sxt.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.entity.SysUser;
import com.sxt.service.SysUserService;

@RestController
public class TestController {

	@Autowired
	private SysUserService service ;
	/**
	 * ResponseEntity spring 提供给为我们的,为了能控制响应码的
	 * 之前:ResultObj
	 * {
	 *   code ; 200
	 *   msg ;
	 *   data ; 
	 * }
	 * @param id
	 * @return
	 */
	@GetMapping("/user/info/{id}")
//	@RequiresRoles("")
//	@RequiresPermissions("")
	public ResponseEntity<SysUser> findById(@PathVariable("id") Long id){
		SysUser user = service.getById(id);
		return ResponseEntity.ok(user);
//	    return ResponseEntity.badRequest().body(user);
	}
}
