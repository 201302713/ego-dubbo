package com.sxt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.anno.ActionLog;
import com.sxt.entity.SysMenu;
import com.sxt.service.SysMenuService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {

	@Autowired
	private SysMenuService sysMenuService ;

	@GetMapping("/table")
	@ApiOperation(value = "查询所有的菜单")
	public ResponseEntity<List<SysMenu>> listAll(){
		List<SysMenu> menus = sysMenuService.list();
		return ResponseEntity.ok(menus) ;
	}

	@GetMapping("/list")
	@ApiOperation(value = "查询所有菜单，但是不包含按钮，因为新增时，需要选择一个父亲，但是按钮不能作为父亲")
	public ResponseEntity<List<SysMenu>> listNoButton(){
		List<SysMenu> menus = sysMenuService.listNoButton();
		return ResponseEntity.ok(menus) ;
	}

	@PostMapping
	@ApiOperation(value = "新增一个菜单")
	public ResponseEntity<Void> add(@RequestBody SysMenu menu){
		sysMenuService.save(menu);
		return ResponseEntity.ok().build() ;
	}

	@GetMapping("/info/{id}")
	@ApiOperation("回显菜单")
	public ResponseEntity<SysMenu> findById(@PathVariable("id") Long id){
		SysMenu sysMenu = sysMenuService.getById(id);
		return ResponseEntity.ok(sysMenu);
	}

	@PutMapping
	@ApiOperation(value = "修改菜单数据")
	public ResponseEntity<Void> update(@RequestBody SysMenu menu){
		sysMenuService.updateById(menu);
		return ResponseEntity.ok().build() ;
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "删除一个菜单数据")
	@ActionLog(value = "删除菜单")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id){
		sysMenuService.removeById(id);
		return ResponseEntity.ok().build() ;
	}
}
