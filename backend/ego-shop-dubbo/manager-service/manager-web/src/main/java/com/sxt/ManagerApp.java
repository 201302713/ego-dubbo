package com.sxt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 后台管理系统的启动类
 * @author WHSXT-LTD
 *
 */
@SpringBootApplication
@MapperScan("com.sxt.mapper")
public class ManagerApp {
 
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ManagerApp.class, args);
	}

}
