package com.sxt.controller.store;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.entity.Area;
import com.sxt.service.AreaService;

import io.swagger.annotations.ApiOperation;

/**
 * 地区的数据接口
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/admin/area")
public class AreaController {

	@Reference(check = false)
	private AreaService areaService ;
	/**
	 * 通过父亲查询儿子
	 */
	@GetMapping("/listByPid")
	@ApiOperation("查询子节点")
	public ResponseEntity<List<Area>> listPyPid(@RequestParam(defaultValue = "0")Long pid){
		List<Area> subArea = areaService.listByPid(pid);
		return ResponseEntity.ok(subArea);
	}

	@GetMapping("/list")
	@ApiOperation("全查询")
	public ResponseEntity<List<Area>> list(){
		List<Area> list = areaService.list();
		return ResponseEntity.ok(list);
	}

	@PostMapping
	@ApiOperation("新增一个区域数据")
	public ResponseEntity<Void> save(@RequestBody Area area){
		areaService.save(area);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/info/{id}")
	@ApiOperation("区域信息的回显")
	public ResponseEntity<Area> findById(@PathVariable("id") Long id){
		Area area = areaService.getById(id);
		return ResponseEntity.ok(area);
	}

	@PutMapping
	@ApiOperation("修改区域信息")
	public ResponseEntity<Void> update(@RequestBody Area area){
		areaService.updateById(area);
		return ResponseEntity.ok().build();
	}
}
