package com.sxt.aspect;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.slf4j.Slf4j;

/**
 * 该切面可以拦截所有的controller的异常
 * @author WHSXT-LTD
 *
 */
@RestControllerAdvice
@Slf4j
public class WebErrorAspect {
 
	/**
	 * 只处理IllegalArgumentException
	 * @return
	 */
	@ExceptionHandler(value = IllegalArgumentException.class)
	public ResponseEntity<String> illegalArgument(IllegalArgumentException e){
		log.error("不合法的参数",e);
		// 给用户弹窗提醒
		return ResponseEntity.badRequest().body(e.getMessage());
	}
}
