package com.sxt.controller.store;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.IndexImg;
import com.sxt.entity.Prod;
import com.sxt.service.IndexImgService;
import com.sxt.service.ProdService;

import io.swagger.annotations.ApiOperation;

/**
 * 轮播图的curd
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/admin/indexImg")
public class IndexImgController {

	@Reference(check = false)
	private IndexImgService  indexImgService ;
	
	@Reference(check = false)
	private ProdService prodService ;

	@GetMapping("/page")
	public ResponseEntity<IPage<IndexImg>> findByPage(
			@RequestParam(defaultValue = "1") Integer current ,
			@RequestParam(defaultValue = "10") Integer size ,
			Integer status
			){
		Page<IndexImg> page = new Page<IndexImg>(current, size);
		IndexImg indexImg = new IndexImg();
		indexImg.setStatus(status);
		IPage<IndexImg> pageData = indexImgService.findByPage(page,indexImg);
		return ResponseEntity.ok(pageData);
	}

	@PostMapping
	@ApiOperation("新增一个轮播图")
	public ResponseEntity<Void> save(@RequestBody IndexImg indexImg){
		indexImgService.save(indexImg);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/info/{id}")
	@ApiOperation("轮播图的回显")
	public ResponseEntity<IndexImg> findById(@PathVariable("id")Long id){
		IndexImg indexImg = indexImgService.getById(id);
		if(indexImg.getRelation()!=null) {
			// prod 里面只要2 个属性pic prodName
			Prod prod = prodService.getProdPicAndProdName(indexImg.getRelation());
			indexImg.setPic(prod.getPic());
			indexImg.setProdName(prod.getProdName());
		}
		
		return ResponseEntity.ok(indexImg) ;
	}
	
	@PutMapping
	@ApiOperation("修改轮播图的数据")
	public ResponseEntity<Void> update(@RequestBody IndexImg indexImg){
		indexImgService.updateById(indexImg);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping
	@ApiOperation("批量的删除")
	public ResponseEntity<Void> batchDelete(@RequestBody List<Long> ids){
		indexImgService.removeByIds(ids);
		return ResponseEntity.ok().build();
	}
}
