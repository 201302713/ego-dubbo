package com.sxt.controller.store;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.PickAddr;
import com.sxt.service.PickAddrService;

import io.swagger.annotations.ApiOperation;

/**
 * 自提点的数据接口
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/shop/pickAddr")
public class PickAddrController {

	@Reference(check = false)
	private PickAddrService addrService ;

	/**
	 * 分页查询自提点
	 */
	@GetMapping("/page")
	@ApiOperation("自提点的分页查询")
	public ResponseEntity<IPage<PickAddr>> page(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size ,
			String addrName
			){
		Page<PickAddr> page = new Page<PickAddr>(current, size);
		PickAddr pickAddr = new PickAddr();
		pickAddr.setAddrName(addrName);
		IPage<PickAddr> pageData = addrService.findByPage(page,pickAddr);
		return ResponseEntity.ok(pageData);
	}
	
	@PostMapping
	@ApiOperation("新增自提点")
	public ResponseEntity<Void> save(@RequestBody PickAddr pickAddr){
		pickAddr.setShopId(1L) ;
		addrService.save(pickAddr);
		return ResponseEntity.ok().build() ;
	}
	
	@GetMapping("/info/{id}")
	@ApiOperation("id 查询")
	public ResponseEntity<PickAddr> findById(@PathVariable("id") Long id){
		PickAddr pickAddr = addrService.getById(id);
		return ResponseEntity.ok(pickAddr);
	}
	
	@PutMapping
	@ApiOperation("修改自提点")
	public ResponseEntity<Void> update(@RequestBody PickAddr pickAddr){
		addrService.updateById(pickAddr);
		return ResponseEntity.ok().build() ;
	}
}
