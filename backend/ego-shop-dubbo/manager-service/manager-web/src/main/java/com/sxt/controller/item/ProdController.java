package com.sxt.controller.item;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.Prod;
import com.sxt.service.ProdService;

import io.swagger.annotations.ApiOperation;

/**
 * 商品相关的数据接口
 * @author WHSXT-LTD
 * 在prodController 里面需要调用ProdService ，是远程调用还是本地调用
 * 远程调用：需要导入api的包
 *
 */
@RestController
@RequestMapping("/prod/prod")
public class ProdController {

	@Reference(check = false) // 在项目启动检查prodService 是否由服务提供者，若没有，直接无法启动
	private ProdService prodService;

	@GetMapping("/page")
	@ApiOperation(value = "分页查询商品的数据")
	public ResponseEntity<IPage<Prod>> findByPage(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10") Integer size ,
			String prodName ,
			Integer status
			){
		Page<Prod> page = new Page<Prod>(current,size);
		Prod prod = new Prod();
		prod.setProdName(prodName);
		prod.setStatus(status);
		IPage<Prod> prodData = prodService.findByPage(page, prod);
		return ResponseEntity.ok(prodData) ;
	}


	@PostMapping
	@ApiOperation("新增一个商品")
	public ResponseEntity<Void> save(@RequestBody Prod prod){
		prodService.save(prod);
		return ResponseEntity.ok().build();
	}

	@ApiOperation("商品数据的回显")
	@GetMapping("/info/{id}")
	public ResponseEntity<Prod>  findById(@PathVariable("id") Long id){
		Prod prod = prodService.getById(id);
		return ResponseEntity.ok(prod);
	}
	
	@PutMapping
	@ApiOperation("修改一个商品")
	public ResponseEntity<Void> update(@RequestBody Prod prod){
		prodService.updateById(prod);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id){
		prodService.removeById(id);
		return ResponseEntity.ok().build();
	}

}
