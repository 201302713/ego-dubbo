package com.sxt.controller;

import java.util.List;

import org.apache.catalina.security.SecurityUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.SysRole;
import com.sxt.entity.SysUser;
import com.sxt.service.SysRoleService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/sys/role")
public class SysRoleContoller {

	@Autowired
	private SysRoleService  sysRoleService ;

	@GetMapping("/list")
	@ApiOperation("全查询角色列表")
	public ResponseEntity<List<SysRole>> list(){
		List<SysRole> roles = sysRoleService.list();
		return ResponseEntity.ok(roles) ;
	}

	@GetMapping("/page")
	@ApiOperation(value = "分页查询角色数据")
	public ResponseEntity<IPage<SysRole>> page(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size,
			String roleName 
			){
		Page<SysRole> page = new Page<SysRole>(current, size);
		SysRole sysRole = new SysRole();
		sysRole.setRoleName(roleName);
		IPage<SysRole> pageData = sysRoleService.findByPage(page,sysRole);
		return ResponseEntity.ok(pageData) ;
	}

	@PostMapping
	@ApiOperation(value = "新增角色")
	public ResponseEntity<Void> save(@RequestBody SysRole sysRole){
		SysUser user = (SysUser)SecurityUtils.getSubject().getPrincipal();
		sysRole.setCreateUserId(user.getUserId());
		sysRoleService.save(sysRole);
		return ResponseEntity.ok().build();
	}


	@GetMapping("/info/{id}")
	@ApiOperation("角色的id查询")
	public ResponseEntity<SysRole> findById(@PathVariable("id") Long id){
		SysRole sysRole = sysRoleService.getById(id);
		return ResponseEntity.ok(sysRole);
	}

	@PutMapping
	@ApiOperation("角色的修改")
	public ResponseEntity<Void> update(@RequestBody SysRole sysRole){
		SysUser user = (SysUser)SecurityUtils.getSubject().getPrincipal();
		sysRole.setCreateUserId(user.getUserId());
		sysRoleService.updateById(sysRole);
		return ResponseEntity.ok().build();
	}
}
