package com.sxt.config;

import java.io.Serializable;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
public class TokenWebSessionManager extends DefaultWebSessionManager{

	private static final String TOKEN_HEADER = "TOKEN" ;
	
	@Override
	protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
		String token = WebUtils.toHttp(request).getHeader(TOKEN_HEADER);
		System.out.println(token);
		if(StringUtils.hasText(token)) { // 代表从头里面取到值
			return token  ;// 将它作为session的id
		}
		token = UUID.randomUUID().toString();
		return token;
	}
}
