package com.sxt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.SysLog;
import com.sxt.service.SysLogService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/sys/log")
public class SysLogController {

	@Autowired
	private SysLogService sysLogService  ; 

	@GetMapping("/page")
	@ApiOperation("分页查询日志记录")
	public ResponseEntity<IPage<SysLog>> findByPage(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size,
			String username,
			String opereation			
			){
		Page<SysLog> page = new Page<SysLog>(current, size);
		SysLog sysLog = new SysLog();
		sysLog.setUsername(username);
		sysLog.setOperation(opereation);
		IPage<SysLog> pageData = sysLogService.findByPage(page , sysLog);
		return ResponseEntity.ok(pageData);

	}
}
