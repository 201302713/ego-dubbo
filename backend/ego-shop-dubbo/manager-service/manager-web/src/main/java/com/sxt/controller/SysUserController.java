package com.sxt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.SysUser;
import com.sxt.service.SysUserService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/sys/user")
public class SysUserController {

	@Autowired
	private SysUserService sysUserService ;
	/**
	 * 分页查询
	 */
	@GetMapping("/page")
	@ApiOperation(value = "分页查询数据")
	public ResponseEntity<IPage<SysUser>> findByPage(
			@RequestParam(defaultValue = "1")Integer current,
			@RequestParam(defaultValue = "10")Integer size,
			String username  // 用户的名称 
			){
		Page<SysUser> page = new Page<SysUser>(current, size); // 对分页参数的处理
		SysUser sysUser = new SysUser(); // 对条件的处理
		sysUser.setUsername(username) ; 
		IPage<SysUser> pageData = sysUserService.findByPage(page,sysUser);
		return ResponseEntity.ok(pageData);
	}

	@GetMapping("/info/{id}")
	@ApiOperation("查询单个用户")
	public ResponseEntity<SysUser> findById(@PathVariable("id") Long id){
		SysUser sysUser = sysUserService.getById(id);
		return ResponseEntity.ok(sysUser);

	}

	@PostMapping
	@ApiOperation(value = "新增一个用户")
	public ResponseEntity<Void> save(@RequestBody SysUser sysUser){
		sysUserService.save(sysUser);
		return ResponseEntity.ok().build();
	}

	
	/**
	 * 批量删除
	 */
	@DeleteMapping
	@ApiOperation(value = "删除一个用户")
	public ResponseEntity<Void> delete(@RequestBody List<Long> ids){
		sysUserService.removeByIds(ids);
		return ResponseEntity.ok().build() ;
	}

	@PutMapping
	@ApiOperation(value = "修改一个用户")
	public ResponseEntity<Void> update(@RequestBody SysUser sysUser){
		sysUserService.updateById(sysUser); // 修改需要考虑中间表
		return ResponseEntity.ok().build();
	}
}
