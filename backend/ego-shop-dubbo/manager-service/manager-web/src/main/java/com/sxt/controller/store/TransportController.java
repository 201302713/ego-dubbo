package com.sxt.controller.store;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.entity.Transport;
import com.sxt.service.TransportService;

import io.swagger.annotations.ApiOperation;

/**
 * 运费模板的数据接口
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/shop/transport")
public class TransportController {

	@Reference(check = false)
	private TransportService transportService ;


	@GetMapping("/page")
	@ApiOperation("分页查询")
	public ResponseEntity<IPage<Transport>> findByPage(
			@RequestParam(defaultValue = "1") Integer current,
			@RequestParam(defaultValue = "10")Integer size,
			String transName
			){
		Page<Transport> page = new Page<Transport>(current,size);
		Transport transport = new Transport();
		transport.setTransName(transName);
		IPage<Transport> pageData = transportService.findByPage(page,transport);
		return ResponseEntity.ok(pageData);
	}
	
	
	@PostMapping
	@ApiOperation("新增一个运费模板")
	public ResponseEntity<Void> save(@RequestBody Transport transport){
		transportService.save(transport);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/info/{id}")
	@ApiOperation("运费模板的回显")
	public ResponseEntity<Transport> findById(@PathVariable("id") Long id){
		Transport transport = transportService.getById(id);
		return ResponseEntity.ok(transport);
	}
}
