package com.sxt.service.impl;

import com.sxt.entity.SysLog;
import com.sxt.mapper.SysLogMapper;
import com.sxt.service.SysLogService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

	@Autowired
	private SysLogMapper sysLogMapper ;
	@Override
	public IPage<SysLog> findByPage(Page<SysLog> page, SysLog sysLog) {
		log.info("分页查询{},{}",page.getCurrent() ,page.getSize());
		return sysLogMapper.selectPage(page, new LambdaQueryWrapper<SysLog>().
				like(
						StringUtils.hasText(sysLog.getUsername()), SysLog::getUsername, sysLog.getUsername()
						).
				like(
						StringUtils.hasText(sysLog.getOperation()), SysLog::getOperation, sysLog.getOperation())
				);
	}

}
