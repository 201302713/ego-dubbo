package com.sxt.service.impl;

import com.sxt.entity.SysRole;
import com.sxt.entity.SysRoleMenu;
import com.sxt.mapper.SysRoleMapper;
import com.sxt.mapper.SysRoleMenuMapper;
import com.sxt.service.SysRoleService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper ;

	@Autowired 
	private SysRoleMenuMapper sysRoleMenuMapper ;


	@Override
	public IPage<SysRole> findByPage(Page<SysRole> page, SysRole sysRole) {
		log.info("分页查询{},{}", page.getCurrent(),page.getSize());
		return sysRoleMapper.selectPage(page, new LambdaQueryWrapper<SysRole>().
				like(StringUtils.hasText(sysRole.getRoleName()),SysRole::getRoleName, sysRole.getRoleName()));

	}

	@Transactional
	public boolean save(SysRole entity) {
		Assert.notNull(entity, "角色不能为null");
		if(!StringUtils.hasText(entity.getRoleName())) {
			throw new IllegalArgumentException("角色名称不能为空");
		}
		entity.setCreateTime(LocalDateTime.now());
		boolean isSave = super.save(entity);
		if(isSave) { // 维护中间表的关系
			List<Object> menuIdList = entity.getMenuIdList() ;
			if(menuIdList!=null && !menuIdList.isEmpty()) {
				for (Object menuIdObj : menuIdList) {
					SysRoleMenu sysRoleMenu = new SysRoleMenu();
					sysRoleMenu.setMenuId(Long.valueOf(menuIdObj.toString()));
					sysRoleMenu.setRoleId(entity.getRoleId());
					sysRoleMenuMapper.insert(sysRoleMenu);
				}
			}
		}
		return isSave;
	}

	@Override
	public SysRole getById(Serializable id) {
		Assert.notNull(id, "id 不能为空");
		log.info("查询id为{}角色",id);
		SysRole sysRole = super.getById(id);
		if(sysRole!=null) {
			List<Object> menuIdList = sysRoleMenuMapper.selectObjs(new LambdaQueryWrapper<SysRoleMenu>().
					select(SysRoleMenu::getMenuId).
					eq(SysRoleMenu::getRoleId, id)            
					);
			sysRole.setMenuIdList(menuIdList) ;
		}
		return sysRole;
	}
	//  <button name ="" value="清除缓存" onclick="javascript(0);alert('清除成功!')">
	
	@Transactional
	@Override
	public boolean updateById(SysRole entity) {
		Assert.notNull(entity, "角色不能为null");
		if(!StringUtils.hasText(entity.getRoleName())) {
			throw new IllegalArgumentException("角色名称不能为空");
		}
		boolean isUpdate = super.updateById(entity);
		if(isUpdate) { // 中间表的维护
			sysRoleMenuMapper.delete(new LambdaQueryWrapper<SysRoleMenu>().
					eq(SysRoleMenu::getRoleId, entity.getRoleId()));
			List<Object> menuIdList = entity.getMenuIdList(); // 新增新的值
			if(menuIdList!=null && !menuIdList.isEmpty()) {
				for (Object menuIdObj : menuIdList) {
					SysRoleMenu sysRoleMenu = new SysRoleMenu();
					sysRoleMenu.setMenuId(Long.valueOf(menuIdObj.toString()));
					sysRoleMenu.setRoleId(entity.getRoleId());
					sysRoleMenuMapper.insert(sysRoleMenu);
				}
			}
		}
		return isUpdate;
	}

}
