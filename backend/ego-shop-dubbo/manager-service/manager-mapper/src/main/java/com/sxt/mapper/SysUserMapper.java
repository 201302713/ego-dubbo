package com.sxt.mapper;

import com.sxt.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
