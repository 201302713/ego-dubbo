package com.sxt.mapper;

import com.sxt.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户与角色对应关系 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
