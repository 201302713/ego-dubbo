package com.sxt.mapper;

import com.sxt.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
