package com.sxt.service;

import com.sxt.entity.SysRole;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysRoleService extends IService<SysRole> {

	/**
	 * 分页查询角色数据
	 * @param page
	 * 分页数据
	 * @param sysRole
	 * 条件
	 * @return
	 */
	IPage<SysRole> findByPage(Page<SysRole> page, SysRole sysRole);

}
