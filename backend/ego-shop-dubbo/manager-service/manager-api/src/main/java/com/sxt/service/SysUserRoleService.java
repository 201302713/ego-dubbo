package com.sxt.service;

import com.sxt.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与角色对应关系 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
