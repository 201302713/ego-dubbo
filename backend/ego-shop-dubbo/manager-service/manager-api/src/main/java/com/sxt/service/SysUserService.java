package com.sxt.service;

import com.sxt.entity.SysUser;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysUserService extends IService<SysUser> {

	/**
	 * 通过用户名查询用户
	 * @param username
	 * 用户的名称
	 * @return
	 * 单个用户对象
	 */
	SysUser findUserByUsername(String username);

	/**
	 * 分页查询用户对象
	 * @param page
	 * @param sysUser
	 * @return
	 */
	IPage<SysUser> findByPage(Page<SysUser> page, SysUser sysUser);

}
