package com.sxt.service;

import com.sxt.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色与菜单对应关系 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
