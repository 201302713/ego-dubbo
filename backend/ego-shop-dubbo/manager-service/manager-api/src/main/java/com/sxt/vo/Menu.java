package com.sxt.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * List<Menu>
 * @author WHSXT-LTD
 *
 */
@Data
public class Menu {
	private String url; // 菜单的url 地址，点击可以跳转
    private List<Menu> list = new ArrayList<Menu>(); // 菜单的子菜单
    private Long menuId ; // 菜单的id
    private String name ;  // 菜单的名称
}
