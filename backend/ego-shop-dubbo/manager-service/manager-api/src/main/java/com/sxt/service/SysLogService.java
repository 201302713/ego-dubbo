package com.sxt.service;

import com.sxt.entity.SysLog;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysLogService extends IService<SysLog> {

	/**
	 * 条件分页查询日志
	 * @param page
	 * @param sysLog 
	 * 条件对象
	 * @return
	 */
	IPage<SysLog> findByPage(Page<SysLog> page, SysLog sysLog);

}
