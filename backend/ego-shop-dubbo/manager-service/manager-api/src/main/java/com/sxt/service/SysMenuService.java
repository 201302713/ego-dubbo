package com.sxt.service;

import com.sxt.entity.SysMenu;
import com.sxt.vo.Authority;
import com.sxt.vo.Menu;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SysMenuService extends IService<SysMenu> {

	/**
	 * 通过用户的id 查询用户的菜单
	 * @param userId
	 * @return
	 */
	List<Menu> findMenusByUser(Long userId);

	/**
	 * 通过用户的id 查询权限
	 * @param userId
	 * @return
	 */
	List<Authority> findAuthByUser(Long userId);

	/**
	 * 查询所有的菜单数据，但是不包含按钮
	 * @return
	 */
	List<SysMenu> listNoButton();

}
