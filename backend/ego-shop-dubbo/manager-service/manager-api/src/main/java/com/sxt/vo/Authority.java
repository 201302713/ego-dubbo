package com.sxt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * List<Authority>
 * @author WHSXT-LTD
 * 前端的每个element 代表该值
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Authority {
	/**
	 * 代表权限
	 */
	private String authority;
}
