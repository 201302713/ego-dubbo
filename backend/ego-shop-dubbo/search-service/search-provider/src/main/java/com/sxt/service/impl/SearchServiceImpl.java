package com.sxt.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.dubbo.config.annotation.Service;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.model.ProdSolrDto;
import com.sxt.service.SearchService;

@Service // 对外暴露服务
public class SearchServiceImpl  implements SearchService{

	/**
	 * 价格
	 */
	public static final int PRICE_ASC  = 2 ;
	public static final int PRICE_DESC = 4 ;

	/**
	 * 销量
	 */
	public static final int SOLD_ASC = 3 ;
	public static final int SOLD_DESC = 1 ;

	/**
	 * 默认
	 */
	public static final int DEFAULT_SORT = 0 ; //好评从大到小 

	@Autowired
	private SolrClient solrClient ;

	@Override
	public Page<ProdSolrDto> searchForTag(Page<ProdSolrDto> page, Long tagId) {
		if(tagId == null) {
			return null ;
		}
		String q = "tag_list:"+tagId ;
		SolrQuery solrQuery = new SolrQuery(q);
		buildePage(solrQuery ,page); // 使用他处理solrQuery 可以带分页的效果
		return search(solrQuery ,page); // 执行搜索
	}


	@Override
	public Page<ProdSolrDto> searchForCategory(Page<ProdSolrDto> page, Long categoryId) {
		if(categoryId == null) {
			return null ;
		}
		String q = "category_id:"+categoryId ;
		SolrQuery solrQuery = new SolrQuery(q);
		buildePage(solrQuery ,page );
		return search(solrQuery , page);
	}

	@Override
	public Page<ProdSolrDto> searchForKeyword(Page<ProdSolrDto> page, String keyword, Integer sort) {
		if(!StringUtils.hasText(keyword)) {
			keyword = "*" ;
		}
		String q = "keyword:"+keyword ;
		SolrQuery solrQuery = new SolrQuery(q);
		buildePage(solrQuery,page);
		buildeSort(solrQuery,sort);
		return search(solrQuery,page);
	}

	/**
	 * 给 搜索条件里面添加分页
	 * @param solrQuery
	 */
	private void buildePage(SolrQuery solrQuery,Page<ProdSolrDto> page) {
		solrQuery.setStart((int)((page.getCurrent() - 1)*page.getSize()) ) ;
		solrQuery.setRows((int)page.getSize()) ; 
	}


	/**
	 * 搞定排序
	 * @param solrQuery
	 * @param sort
	 */
	private void buildeSort(SolrQuery solrQuery, Integer sort) {
		if(sort == null) {
			sort = DEFAULT_SORT ;
		}
		switch (sort) {
		case PRICE_ASC:
			solrQuery.setSort("price", ORDER.asc);
			break;
		case PRICE_DESC:
			solrQuery.setSort("price", ORDER.desc);
			break;
		case SOLD_ASC:
			solrQuery.setSort("sold_num", ORDER.asc);
			break;
		case SOLD_DESC:
			solrQuery.setSort("sold_num", ORDER.desc);
			break;
		case DEFAULT_SORT:
			solrQuery.setSort("positive_rating", ORDER.desc);
			break;
		default:
			throw new RuntimeException("不支持的排序类型") ;
		}
	}


	private Page<ProdSolrDto> search(SolrQuery solrQuery,Page<ProdSolrDto> page) {
		try {
			QueryResponse queryResponse = solrClient.query(solrQuery) ;
			SolrDocumentList results = queryResponse.getResults();
			// 1 得到总条数
			long numFound = results.getNumFound();
			page.setTotal(numFound) ;
			// 2 处理结果集
			DocumentObjectBinder documentObjectBinder = new DocumentObjectBinder();
			// results solr的结果,他的字段类型有问题 , double->bigDeci 类型有问题
			List<ProdSolrDto> prodDtoSolrs = new ArrayList<ProdSolrDto>(results.size());
			for (SolrDocument solrDocument : results) {
				BigDecimal price = new BigDecimal(solrDocument.getFieldValue("price").toString());
				BigDecimal positivRating = new BigDecimal(solrDocument.getFieldValue("positive_rating").toString());
				solrDocument.setField("price", price);
				solrDocument.setField("positive_rating", positivRating);
				ProdSolrDto prodSolrDto = documentObjectBinder.getBean(ProdSolrDto.class, solrDocument);
				prodDtoSolrs.add(prodSolrDto) ;
			}
			page.setRecords(prodDtoSolrs) ;
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return page;
	}


}
