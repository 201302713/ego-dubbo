package com.sxt;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableDubbo
@MapperScan("com.sxt.mapper")
@SpringBootApplication
@EnableScheduling
public class SearchApp {
  public static void main(String[] args) throws Exception {
	SpringApplication.run(SearchApp.class, args);
}

}
