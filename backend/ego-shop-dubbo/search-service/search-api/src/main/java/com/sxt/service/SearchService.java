package com.sxt.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sxt.model.ProdSolrDto;

/**
 * 用户实现搜索的接口
 * @author WHSXT-LTD
 *
 */
public interface SearchService {

	/**
	 * 通过活动/标签的id 查询所对应的商品
	 * @param page
	 * 分页
	 * @param tagId
	 * 标签的id
	 * @return
	 */
	Page<ProdSolrDto> searchForTag(Page<ProdSolrDto> page,Long tagId);
	
	/**
	 * 通过分类的id 来查询商品
	 * @param page
	 * @param categoryId
	 * @return
	 */
	Page<ProdSolrDto> searchForCategory(Page<ProdSolrDto> page,Long categoryId);
	
	/**
	 * 通过关键字搜索商品
	 * @param page
	 *  分页
	 * @param keyword
	 * 关键字
	 * @param sort
	 * 排序的类型
	 * @return
	 */
	Page<ProdSolrDto> searchForKeyword(Page<ProdSolrDto> page,String keyword,Integer sort);
	
}
