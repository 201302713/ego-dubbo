package com.sxt.service;

import com.sxt.entity.HotSearch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 热搜 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface HotSearchService extends IService<HotSearch> {

}
