package com.sxt.service;

import java.util.List;

import com.sxt.model.ShopCartItem;

/**
 * 数据的导入
 * @author WHSXT-LTD
 *
 */
public interface ImportProdService {

	/**
	 * 导入所有的数据
	 * 全量导入
	 */
	public void importAllProd();
	
	/**
	 * 导入修改的,新增的数据
	 * 增量的导入
	 */
	public void importIncrementProd();
	
	/**
	 * 实时的导入
	 * 要求数据要里面更新的导入
	 */
	public void importListOrderItem(List<ShopCartItem> cartItems);
}
