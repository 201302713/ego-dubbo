package com.sxt.mapper;

import com.sxt.entity.HotSearch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 热搜 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface HotSearchMapper extends BaseMapper<HotSearch> {

}
