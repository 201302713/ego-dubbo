package com.sxt.mapper;

import com.sxt.entity.IndexImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 主页轮播图 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface IndexImgMapper extends BaseMapper<IndexImg> {

}
