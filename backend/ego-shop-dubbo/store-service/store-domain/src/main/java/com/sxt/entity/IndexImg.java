package com.sxt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 主页轮播图
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="IndexImg对象", description="主页轮播图")
public class IndexImg implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	@TableId(value = "img_id", type = IdType.AUTO)
	private Long imgId;

	@ApiModelProperty(value = "店铺ID")
	private Long shopId;

	@ApiModelProperty(value = "图片")
	private String imgUrl;

	@ApiModelProperty(value = "说明文字,描述")
	private String des;

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "链接")
	private String link;

	// 只有数据库tinyint = Boolean 0/1
	@ApiModelProperty(value = "状态")
	private Integer status;

	@ApiModelProperty(value = "顺序")
	private Integer seq;

	@ApiModelProperty(value = "上传时间")
	private LocalDateTime uploadTime;

	@ApiModelProperty(value = "关联")
	private Long relation;

	@ApiModelProperty(value = "类型")
	private Integer type;
	/**
	 * 相关联的商品的图片
	 */
	@ApiModelProperty("相关联商品的图片")
	@TableField(exist = false)
	private String pic ;
	/**
	 * 关联的商品名称
	 */
	@ApiModelProperty("相关联商品的名称")
	@TableField(exist = false)
	private String prodName;

}
