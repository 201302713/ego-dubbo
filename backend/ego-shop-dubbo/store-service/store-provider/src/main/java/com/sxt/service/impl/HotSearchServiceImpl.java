package com.sxt.service.impl;

import com.sxt.entity.HotSearch;
import com.sxt.mapper.HotSearchMapper;
import com.sxt.service.HotSearchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 * 热搜 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class HotSearchServiceImpl extends ServiceImpl<HotSearchMapper, HotSearch> implements HotSearchService {

}
