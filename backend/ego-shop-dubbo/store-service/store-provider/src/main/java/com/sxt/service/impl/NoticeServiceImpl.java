package com.sxt.service.impl;

import com.sxt.entity.Notice;
import com.sxt.mapper.NoticeMapper;
import com.sxt.service.NoticeService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "com.sxt.service.impl.NoticeServiceImpl") // @CacheConfig 放在类上,这个类里面方法的缓存的前缀就是cacheNames
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

	@Autowired
	private NoticeMapper noticeMapper ;

	private static final String INDEX_NOTICE = "'all-index-notices'" ;

	@Cacheable(key = INDEX_NOTICE)
	public List<Notice> listAllViewNotice() {
		log.info("加载启用的通知");
		return noticeMapper.selectList(new LambdaQueryWrapper<Notice>().
				select(Notice.class, (tableFiled)->{
					if("content".equals(tableFiled.getColumn())) {
						return false ;
					}
					return true ;
				}).
				eq(Notice::getStatus, Boolean.TRUE).
				orderByDesc(Notice::getPublishTime)
				);
	}
}
