package com.sxt;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableDubbo
@MapperScan("com.sxt.mapper")
@EnableCaching
public class StoreApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(StoreApp.class, args);
	}

}
