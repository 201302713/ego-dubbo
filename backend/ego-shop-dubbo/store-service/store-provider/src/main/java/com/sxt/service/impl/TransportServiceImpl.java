package com.sxt.service.impl;

import com.sxt.entity.Area;
import com.sxt.entity.Transcity;
import com.sxt.entity.TranscityFree;
import com.sxt.entity.Transfee;
import com.sxt.entity.TransfeeFree;
import com.sxt.entity.Transport;
import com.sxt.mapper.AreaMapper;
import com.sxt.mapper.TranscityFreeMapper;
import com.sxt.mapper.TranscityMapper;
import com.sxt.mapper.TransfeeFreeMapper;
import com.sxt.mapper.TransfeeMapper;
import com.sxt.mapper.TransportMapper;
import com.sxt.service.TransportService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service(timeout = 10000)
@Slf4j
public class TransportServiceImpl extends ServiceImpl<TransportMapper, Transport> implements TransportService {

	@Autowired 
	private TransportMapper transportMapper;

	@Autowired
	private TransfeeMapper transfeeMappert;

	@Autowired
	private TransfeeFreeMapper transfeeFreeMapper ;

	@Autowired 
	private TranscityMapper transcityMapper ;
	@Autowired
	private TranscityFreeMapper transcityFreeMapper ;

	@Autowired
	private AreaMapper areaMapper;

	@Override
	public IPage<Transport> findByPage(Page<Transport> page, Transport transport) {
		log.info("分页查询{},{}",page.getCurrent(),page.getSize());
		return transportMapper.selectPage(page, new LambdaQueryWrapper<Transport>().
				like(StringUtils.hasText(transport.getTransName()), Transport::getTransName, transport.getTransName()));
	}

	@Transactional
	public boolean save(Transport entity) {
		Assert.notNull(entity,"运费模板不能为null");
		// 新增一个物流模板
		boolean save = super.save(entity);
		if(save) {
			// 处理收费的标准的写入
			addFee(entity);
			// 处理免费的标准的写入
			addFreeFee(entity);
		}
		return save ;
	}

	/**
	 * 处理收费的表
	 * @param entity
	 */
	private void addFee(Transport entity) {
		List<Transfee> transfees = entity.getTransfees(); // 收费的标准
		for (Transfee transfee : transfees) {
			transfee.setTransportId(entity.getTransportId());
			int result = transfeeMappert.insert(transfee);
			if(result>0) { // 维护中间表
				addTranscity(transfee);
			}
		}
	}
	/**
	 * 处理收费的城市
	 * @param transfee
	 */
	private void addTranscity(Transfee transfee) {
		List<Area> cityList = transfee.getCityList();
		for (Area area : cityList) {
			Transcity transcity = new Transcity(); // 处理中间表
			transcity.setCityId(area.getAreaId());
			transcity.setTransfeeId(transfee.getTransfeeId()) ;
			transcityMapper.insert(transcity);
		}
	}

	/**
	 * 处理包邮的标准
	 * @param entity
	 */
	private void addFreeFee(Transport entity) {
		if(entity.getIsFreeFee()==1) {
			return ;
		}
		if(entity.getHasFreeCondition()!=1) {
			return ;
		}
		List<TransfeeFree> transfeeFrees = entity.getTransfeeFrees();
		for (TransfeeFree transfeeFree : transfeeFrees) {
			transfeeFree.setTransportId(entity.getTransportId());
			int result = transfeeFreeMapper.insert(transfeeFree);
			if(result>0) {
				addTranscityFree(transfeeFree);
			}
		}

	}

	/**
	 * 处理包邮的城市
	 * @param transfeeFree
	 */
	private void addTranscityFree(TransfeeFree transfeeFree) {
		List<Area> freeCityList = transfeeFree.getFreeCityList();
		for (Area area : freeCityList) {
			TranscityFree transcityFree = new TranscityFree();
			transcityFree.setFreeCityId(area.getAreaId());
			transcityFree.setTransfeeFreeId(transfeeFree.getTransfeeFreeId());
			transcityFreeMapper.insert(transcityFree) ;
		}
	}


	/**
	 * 一次性操作6 个表
	 */
	@Override
	public Transport getById(Serializable id) {
		log.info("查询id{}的运费模板",id);
		Transport transport = super.getById(id);
		if(transport!=null) {
			transport.setTransfees(getTransFees(transport));
			transport.setTransfeeFrees(getTransfeeFrees(transport));
		}
		return transport ;
	}

	/**
	 * 根据物流的模板 查询该模板里面 的收费标准
	 * @param id
	 * @return
	 */
	private List<Transfee> getTransFees(Transport transport) {
		List<Transfee> transfees = transfeeMappert.selectList(new LambdaQueryWrapper<Transfee>().eq(Transfee::getTransportId, transport.getTransportId()));
		if(transfees!=null && !transfees.isEmpty()) {
			for (Transfee transfee : transfees) {
				transfee.setCityList(getCityList(transfee));
			}
		}
		return transfees == null ? Collections.emptyList():transfees;
	}
	/**
	 * 查询收费的城市
	 * @param transfee
	 * @return
	 */
	private List<Area> getCityList(Transfee transfee) {
		List<Object> cityIds = transcityMapper.selectObjs(
				new LambdaQueryWrapper<Transcity>().
				select(Transcity::getCityId).
				eq(Transcity::getTransfeeId, transfee.getTransfeeId()));
		if(cityIds==null || cityIds.isEmpty()) {
			return Collections.emptyList() ;
		}
		return areaMapper.selectList(new LambdaQueryWrapper<Area>().in(Area::getAreaId, cityIds));
	}

	/**
	 * 根据物流的模板 查询该模板里面 的包邮标准
	 * @param id
	 * @return
	 */
	private List<TransfeeFree> getTransfeeFrees(Transport transport) {
		if(transport.getHasFreeCondition()==0) {
			return Collections.emptyList() ;
		}
		List<TransfeeFree> transfeeFrees = transfeeFreeMapper.selectList(new LambdaQueryWrapper<TransfeeFree>().eq(TransfeeFree::getTransportId,transport.getTransportId()));
	    if(transfeeFrees==null || transfeeFrees.isEmpty()) {
	    	return Collections.emptyList() ;
	    }
	    for (TransfeeFree transfeeFree : transfeeFrees) {
	    	transfeeFree.setFreeCityList(getFreeCityList(transfeeFree));
		}
		return transfeeFrees;
	}

	/**
	 * 查询包邮的城市
	 * @param transfeeFree
	 * @return
	 */
	private List<Area> getFreeCityList(TransfeeFree transfeeFree) {
		List<Object> cityObjIds = transcityFreeMapper.selectObjs(
				new LambdaQueryWrapper<TranscityFree>().
				select(TranscityFree::getFreeCityId).
				eq(TranscityFree::getTransfeeFreeId, transfeeFree.getTransfeeFreeId())
				);
		if(cityObjIds==null || cityObjIds.isEmpty()) {
			return Collections.emptyList() ;
		}
		
		return  areaMapper.selectList(new LambdaQueryWrapper<Area>().in(Area::getAreaId, cityObjIds));
	}

}
