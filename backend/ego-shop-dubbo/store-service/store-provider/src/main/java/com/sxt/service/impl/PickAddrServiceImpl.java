package com.sxt.service.impl;

import com.sxt.entity.PickAddr;
import com.sxt.mapper.PickAddrMapper;
import com.sxt.service.PickAddrService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 用户配送地址 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class PickAddrServiceImpl extends ServiceImpl<PickAddrMapper, PickAddr> implements PickAddrService {

	@Autowired
	private PickAddrMapper pickAddrMapper ;
	
	@Override
	public IPage<PickAddr> findByPage(Page<PickAddr> page, PickAddr pickAddr) {
		log.info("分页查询{},{}",page.getCurrent(),page.getSize());
		return pickAddrMapper.selectPage(page, new LambdaQueryWrapper<PickAddr>().
				like(StringUtils.hasText(pickAddr.getAddrName()),PickAddr::getAddrName, pickAddr.getAddrName()));
	}

}
