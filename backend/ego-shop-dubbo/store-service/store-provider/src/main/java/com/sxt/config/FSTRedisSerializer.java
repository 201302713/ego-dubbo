package com.sxt.config;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import com.sxt.utils.FstSerializationUtils;

/**
 * 使用redis的序列化
 * @author WHSXT-LTD
 *
 */
public class FSTRedisSerializer implements RedisSerializer<Object>{

	@Override
	public byte[] serialize(Object object) throws SerializationException {
		return FstSerializationUtils.serialization(object);
	}

	@Override
	public Object deserialize(byte[] bytes) throws SerializationException {
		return FstSerializationUtils.deSerialization(bytes);
	}

}
