package com.sxt.config;

import java.time.Duration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair;

@Configuration
public class CacheConfig {

	@Bean
	public RedisCacheConfiguration redisCacheConfiguration() {
		org.springframework.data.redis.cache.RedisCacheConfiguration config = org.springframework.data.redis.cache.RedisCacheConfiguration
				.defaultCacheConfig();
		// redis 将缓存以何种形式序列化 // 默认为jdk new JdkSerializationRedisSerializer(CacheConfig.class.getClassLoader()
		config = config.serializeValuesWith(
				SerializationPair.fromSerializer(new FSTRedisSerializer()));
		// key的过期时间 ,和redis的机器的内存相关,内存越大,时间可以越长,内存越小,时间可以短
		config = config.entryTtl(Duration.ofHours(7));
		// key的前缀和后缀,都走默认的设置,就是前缀读取的时cachenames
		return config;
	}
}
