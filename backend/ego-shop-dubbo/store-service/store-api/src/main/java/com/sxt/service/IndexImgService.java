package com.sxt.service;

import com.sxt.entity.IndexImg;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 主页轮播图 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface IndexImgService extends IService<IndexImg> {

	/**
	 * 分页查询
	 * @param page
	 * @param indexImg
	 * @return
	 */
	IPage<IndexImg> findByPage(Page<IndexImg> page, IndexImg indexImg);

	/**
	 * 加载首页的数据
	 * @return
	 */
	List<IndexImg> loadIndexImgs();

}
