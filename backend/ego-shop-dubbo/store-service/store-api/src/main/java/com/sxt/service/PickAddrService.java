package com.sxt.service;

import com.sxt.entity.PickAddr;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户配送地址 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface PickAddrService extends IService<PickAddr> {

	/**
	 * 分页查询
	 * @param page
	 * @param pickAddr
	 * @return
	 */
	IPage<PickAddr> findByPage(Page<PickAddr> page, PickAddr pickAddr);

}
