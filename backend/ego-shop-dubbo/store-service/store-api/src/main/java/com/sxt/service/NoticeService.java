package com.sxt.service;

import com.sxt.entity.Notice;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface NoticeService extends IService<Notice> {

	/**
	 * 加载启用的,按照发布时间排序的通知集合
	 * @return
	 */
	List<Notice> listAllViewNotice();

}
