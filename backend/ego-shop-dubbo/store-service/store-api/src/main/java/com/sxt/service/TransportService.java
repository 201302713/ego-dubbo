package com.sxt.service;

import com.sxt.entity.Transport;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface TransportService extends IService<Transport> {

	/**
	 * 分页查询数据
	 * @param page
	 * @param transport
	 * @return
	 */
	IPage<Transport> findByPage(Page<Transport> page, Transport transport);

}
