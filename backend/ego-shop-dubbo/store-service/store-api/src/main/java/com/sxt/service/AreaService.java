package com.sxt.service;

import com.sxt.entity.Area;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface AreaService extends IService<Area> {

	/**
	 * 通过父pid 查询子节点
	 * @param pid
	 * @return
	 */
	List<Area> listByPid(Long pid);

}
