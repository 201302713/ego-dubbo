package com.sxt.service.impl;

import com.sxt.entity.ProdProp;
import com.sxt.entity.ProdPropValue;
import com.sxt.mapper.ProdPropMapper;
import com.sxt.mapper.ProdPropValueMapper;
import com.sxt.service.ProdPropService;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class ProdPropServiceImpl extends ServiceImpl<ProdPropMapper, ProdProp> implements ProdPropService {

	@Autowired
	private ProdPropMapper  prodPropMapper ;

	@Autowired
	private ProdPropValueMapper prodPropValueMapper ;

	@Override
	public IPage<ProdProp> findByPage(Page<ProdProp> page, ProdProp prodProp) {
		log.info("分页查询{},{}", page.getCurrent(),page.getSize());

		IPage<ProdProp> pageData = prodPropMapper.selectPage(page, new LambdaQueryWrapper<ProdProp>().
				like(StringUtils.hasText(prodProp.getPropName()), ProdProp::getPropName, prodProp.getPropName())
				);
		List<ProdProp> records = pageData.getRecords();
		if(records!=null && !records.isEmpty()) {
			for (ProdProp prodPropEach : records) {
				List<ProdPropValue> prodPropValues = prodPropValueMapper.selectList(new LambdaQueryWrapper<ProdPropValue>().eq(ProdPropValue::getPropId, prodPropEach.getPropId()));
				prodPropEach.setProdPropValues(prodPropValues); // 这里面获取的prodPropEach 对象的引用，引用会直接修改值
			}
		}
		return pageData ;
	}

	@Transactional
	public boolean save(ProdProp entity) {
		Assert.notNull(entity,"新增的属性值不能为空");
		log.info("新增一个商品的属性:{}",JSONUtil.toJsonStr(entity));
		boolean isSave = super.save(entity);
		if(isSave) {
			List<ProdPropValue> prodPropValues = entity.getProdPropValues();
			if(prodPropValues!=null && !prodPropValues.isEmpty()) {
				for (ProdPropValue prodPropValue : prodPropValues) {
					prodPropValue.setValueId(null); // 前端里面偶而有该值，抹除掉
					prodPropValue.setPropId(entity.getPropId()); // 设置属性的id
					prodPropValueMapper.insert(prodPropValue) ;
				}
			}
		}
		return isSave;
	}

	@Transactional
	public boolean updateById(ProdProp entity) {
		Assert.notNull(entity,"修改属性不能为空");
		Assert.notNull(entity.getPropId(),"修改属性时，id不能为空");
		log.info("修改一个商品的属性:{}",JSONUtil.toJsonStr(entity));
		boolean isUpdate = super.updateById(entity); // 修改属性的表
		if(isUpdate) { // 维护多方的关系表
			//1 删除旧值
			prodPropValueMapper.delete(new LambdaQueryWrapper<ProdPropValue>().eq(ProdPropValue::getPropId, entity.getPropId()));
			// 新增新的值
			List<ProdPropValue> prodPropValues = entity.getProdPropValues();
			if(prodPropValues!=null && !prodPropValues.isEmpty()) {
				for (ProdPropValue prodPropValue : prodPropValues) {
					prodPropValue.setValueId(null); // 前端里面偶而有该值，抹除掉
					prodPropValue.setPropId(entity.getPropId()); // 设置属性的id
					prodPropValueMapper.insert(prodPropValue) ;
				}
			}
		}
		return isUpdate;
	}


	@Transactional
	public boolean removeById(Serializable id) {
		Assert.notNull(id,"删除时id 不能为空");
		boolean isDelete = super.removeById(id);
		if(isDelete) {
			prodPropValueMapper.delete(new LambdaQueryWrapper<ProdPropValue>().eq(ProdPropValue::getPropId, id));
		}
		return isDelete ;
	}

	@Override
	public List<ProdPropValue> listPropValues(Long id) {
		Assert.notNull(id, "属性的id不能为空");
		log.info("查询属性id{}的属性值",id);
		return prodPropValueMapper.selectList
				(new LambdaQueryWrapper<ProdPropValue>().
						  eq(ProdPropValue::getPropId, id));
	}
}
