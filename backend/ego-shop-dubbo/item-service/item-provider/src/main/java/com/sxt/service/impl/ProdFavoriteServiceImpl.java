package com.sxt.service.impl;

import com.sxt.entity.ProdFavorite;
import com.sxt.mapper.ProdFavoriteMapper;
import com.sxt.service.ProdFavoriteService;

import org.apache.dubbo.config.annotation.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 商品收藏表 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class ProdFavoriteServiceImpl extends ServiceImpl<ProdFavoriteMapper, ProdFavorite> implements ProdFavoriteService {

}
