package com.sxt.service.impl;

import com.sxt.entity.ProdTagReference;
import com.sxt.mapper.ProdTagReferenceMapper;
import com.sxt.service.ProdTagReferenceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
public class ProdTagReferenceServiceImpl extends ServiceImpl<ProdTagReferenceMapper, ProdTagReference> implements ProdTagReferenceService {

}
