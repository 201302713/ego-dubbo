package com.sxt.service.impl;

import com.sxt.entity.ProdTag;
import com.sxt.mapper.ProdTagMapper;
import com.sxt.service.ProdTagService;

import lombok.extern.slf4j.Slf4j;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.io.Serializable;
import java.util.List;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 商品分组表 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "com.sxt.service.impl.ProdTagServiceImpl")
public class ProdTagServiceImpl extends ServiceImpl<ProdTagMapper, ProdTag> implements ProdTagService {

	@Autowired
	private   ProdTagMapper prodTagMapper ;
	
	
	private static final  String INDEX_TAGS =  "'all-index-tags'" ;

	@Override
	public IPage<ProdTag> findByPage(Page<ProdTag> page, ProdTag prodTag) {
		log.info("分页查询{},{}",page.getCurrent(),page.getSize());
		return prodTagMapper.selectPage(page, new LambdaQueryWrapper<ProdTag>().
				like(StringUtils.hasText(prodTag.getTitle()), ProdTag::getTitle, prodTag.getTitle()).
				eq(prodTag.getStatus()!=null,ProdTag::getStatus , prodTag.getStatus())
				);
	}

	@Override
	public List<ProdTag> list() {
		log.info("全查询");
		//status = 1 不是status !=0
		// = 在数据库查询时，会走索引 B+Tree
		return prodTagMapper.selectList(new LambdaQueryWrapper<ProdTag>().eq(ProdTag::getStatus, 1));
	}
   
	@CacheEvict(key = INDEX_TAGS)
	public boolean save(ProdTag entity) {
		return super.save(entity);
	}
	
	@CacheEvict(key = INDEX_TAGS)
	public boolean updateById(ProdTag entity) {
		return super.updateById(entity);
	}
	
	@CacheEvict(key = INDEX_TAGS)
	public boolean removeById(Serializable id) {
		return super.removeById(id);
	}
	
	@Cacheable(key = INDEX_TAGS)
	public List<ProdTag> listAllProdTag() {
		return prodTagMapper.selectList(new LambdaQueryWrapper<ProdTag>().
				    eq(ProdTag::getStatus, 1).
				    orderByAsc(ProdTag::getSeq)
				    );
	}

}
