package com.sxt;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 启动类
 * @author WHSXT-LTD
 *
 */
@SpringBootApplication
@EnableDubbo // 开启dubbo的调用
@MapperScan("com.sxt.mapper")
@EnableCaching
public class ItemApp {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ItemApp.class, args);
	}

}
