package com.sxt.service.impl;

import com.sxt.domain.ProdCommonData;
import com.sxt.entity.Prod;
import com.sxt.entity.ProdComm;
import com.sxt.mapper.ProdCommMapper;
import com.sxt.mapper.ProdMapper;
import com.sxt.service.ProdCommService;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.scenario.effect.impl.prism.PrDrawable;

/**
 * <p>
 * 商品评论 服务实现类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Service
@Slf4j
public class ProdCommServiceImpl extends ServiceImpl<ProdCommMapper, ProdComm> implements ProdCommService {

	@Autowired
	private ProdCommMapper prodCommMapper ;
	@Autowired
	private ProdMapper prodMapper;

	@Override
	public IPage<ProdComm> findByPage(Page<ProdComm> page, ProdComm prodComm) {
		log.info("查询{},{}",page.getCurrent(),page.getSize());
		// 1 通过商品的名称来查询
		Map<Long,String> prodIds  = new HashMap<Long,String>(0); // key: 商品的id value：商品的名称
		if(StringUtils.hasText(prodComm.getProdName())) { // 代表用户通过商品名称来模糊查询
			List<Prod> prods = prodMapper.selectList(new LambdaQueryWrapper<Prod>(). // 他只能显示第一个id 值
					select(Prod::getProdId,Prod::getProdName)
					.like(Prod::getProdName, prodComm.getProdName())
					.ne(Prod::getStatus, -1)
					);
			if(prods!=null && !prods.isEmpty()) {
				for (Prod prod : prods) {
					prodIds.put(prod.getProdId(), prod.getProdName());
				}
			}else {
				return null ; // 若前端使用商品名称查询评论，结果根据商品名称查询商品为null，既然没有商品，就没有商品的评论
			}
		}
		// 2 状态prodComm 里面就有状态
		IPage<ProdComm> prodCommon = prodCommMapper.selectPage(page, 
				new LambdaQueryWrapper<ProdComm>().
				eq(!(prodComm.getEvaluate().equals(-1) || prodComm.getEvaluate().equals(3)),ProdComm::getEvaluate, prodComm.getEvaluate())
				.eq(prodComm.getProdId()!=null , ProdComm::getProdId ,prodComm.getProdId())
				.isNotNull(prodComm.getEvaluate().equals(3),ProdComm::getPics).
				eq(prodComm.getStatus()!=null, ProdComm::getStatus, prodComm.getStatus()).
				// mybatis-plus 有bug 在in ，不会先判断prodIds!=null，而是直接循环prodIds.foreache(k,v)
				in(!prodIds.isEmpty(),ProdComm::getProdId,prodIds.keySet()) 
				) ;
		List<ProdComm> records = prodCommon.getRecords();
		if(records!=null && !records.isEmpty()) {
			for (ProdComm prodCommEach : records) {
				String prodName = prodIds.get(prodCommEach.getProdId());
				if(prodName==null) { // 代表没有从Map 集合里面获取到商品名称的值
					// 现在有商品的id，但是没有商品的名称
					List<Object> objs = prodMapper.selectObjs(new LambdaQueryWrapper<Prod>().select(Prod::getProdName).eq(Prod::getProdId, prodCommEach.getProdId()));
					if(objs!=null && !objs.isEmpty()) {
						prodName = objs.get(0).toString() ;// 最多有1个值，因为是通过id 查询
					}
				}
				prodCommEach.setProdName(prodName);
			}
		}
		return prodCommon;
	}

	@Override
	public boolean updateById(ProdComm entity) {
		Assert.notNull(entity,"要修改的数据不能为空");
		if(entity.getProdCommId()==null) {
			throw new IllegalArgumentException("id不能为空");

		}
		entity.setReplySts(1);
		entity.setReplyTime(LocalDateTime.now());
		return super.updateById(entity);
	}

	@Override
	public ProdCommonData loadProdCommData(Long prodId) {
		log.info("查询商品{},的评论信息",prodId);
		Integer totalNum = prodCommMapper.selectCount(new LambdaQueryWrapper<ProdComm>().
				eq(ProdComm::getProdId, prodId)) ;
		//(0好评 1中评 2差评)")
		Integer praiseNumber = prodCommMapper.selectCount(new LambdaQueryWrapper<ProdComm>().
				eq(ProdComm::getProdId, prodId)
				.eq(ProdComm::getEvaluate, 0)
				) ;
		Integer secondaryNumber = prodCommMapper.selectCount(new LambdaQueryWrapper<ProdComm>().
				eq(ProdComm::getProdId, prodId)
				.eq(ProdComm::getEvaluate, 1)
				) ;
		Integer negativeNumber = prodCommMapper.selectCount(new LambdaQueryWrapper<ProdComm>().
				eq(ProdComm::getProdId, prodId)
				.eq(ProdComm::getEvaluate, 2)
				) ;
		Integer pic = prodCommMapper.selectCount(new LambdaQueryWrapper<ProdComm>().
				eq(ProdComm::getProdId, prodId)
				.isNotNull(ProdComm::getPics)
				) ;
		BigDecimal  positiveRating  =  totalNum.equals(0) ?new BigDecimal("0.00") : totalNum.equals(0)? new BigDecimal("0.00") :
			new BigDecimal(praiseNumber).divide(new BigDecimal(totalNum), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
		
		return ProdCommonData.builder()
				.number(totalNum==null?0L:totalNum)
				.positiveRating(positiveRating)
				.picNumber(pic==null?0L:pic)
				.secondaryNumber(secondaryNumber==null?0L:secondaryNumber)
				.negativeNumber(negativeNumber==null?0L:negativeNumber)
				.praiseNumber(praiseNumber==null?0L:praiseNumber)
				.build();
	}

}
