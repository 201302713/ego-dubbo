package com.sxt.mapper;

import com.sxt.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品类目 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
