package com.sxt.mapper;

import com.sxt.entity.Sku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 单品SKU表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SkuMapper extends BaseMapper<Sku> {

}
