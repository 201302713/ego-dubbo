package com.sxt.mapper;

import com.sxt.entity.Prod;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdMapper extends BaseMapper<Prod> {

}
