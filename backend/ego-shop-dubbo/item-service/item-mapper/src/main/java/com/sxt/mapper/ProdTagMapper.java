package com.sxt.mapper;

import com.sxt.entity.ProdTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品分组表 Mapper 接口
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdTagMapper extends BaseMapper<ProdTag> {

}
