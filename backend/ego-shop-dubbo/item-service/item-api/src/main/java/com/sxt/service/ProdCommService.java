package com.sxt.service;

import com.sxt.domain.ProdCommonData;
import com.sxt.entity.ProdComm;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品评论 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdCommService extends IService<ProdComm> {

	/**
	 * 商品分页数据的查询
	 * @param page
	 * @param prodComm
	 * @return
	 */
	IPage<ProdComm> findByPage(Page<ProdComm> page, ProdComm prodComm);
	
	/**
	 * 使用商品的id 查询该商品的评论信息
	 * @param prodId
	 *  商品的id
	 * @return
	 */
	ProdCommonData loadProdCommData(Long prodId);

}
