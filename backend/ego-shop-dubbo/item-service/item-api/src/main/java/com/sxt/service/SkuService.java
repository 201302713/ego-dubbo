package com.sxt.service;

import com.sxt.entity.Sku;
import com.sxt.model.ShopCartItem;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 单品SKU表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface SkuService extends IService<Sku> {

	void decrStock(List<ShopCartItem> cartItems);

}
