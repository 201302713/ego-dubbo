package com.sxt.service;

import com.sxt.entity.Category;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 产品类目 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface CategoryService extends IService<Category> {

	/**
	 * 查询父菜单
	 * @return
	 */
	List<Category> listParentCategory();

	/**
	 * 查询所有的二级菜单
	 * @return
	 */
	List<Category> getSubCateList();

}
