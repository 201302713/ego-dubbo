package com.sxt.service;

import com.sxt.entity.ProdTagReference;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdTagReferenceService extends IService<ProdTagReference> {

}
