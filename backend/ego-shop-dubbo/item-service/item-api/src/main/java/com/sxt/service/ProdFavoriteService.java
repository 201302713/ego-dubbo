package com.sxt.service;

import com.sxt.entity.ProdFavorite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品收藏表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdFavoriteService extends IService<ProdFavorite> {

}
