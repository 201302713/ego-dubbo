package com.sxt.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProdCommonData implements Serializable {/**
 * 
 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("好评率")
	private BigDecimal positiveRating ;
	
	@ApiModelProperty("总的评论数")
	private Long number  ;
	
	@ApiModelProperty("好评数")
	private Long praiseNumber ;
	
	@ApiModelProperty("中评数")
	private Long secondaryNumber ;
	
	@ApiModelProperty("差评数")
	private Long negativeNumber ;
	
	@ApiModelProperty("有图评数")
	private Long picNumber ;

}
