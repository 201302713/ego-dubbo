package com.sxt.service;

import com.sxt.entity.Prod;
import com.sxt.model.ProdSolrDto;
import com.sxt.model.ShopCartItem;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdService extends IService<Prod> {

	/**
	 * 分页查询商品数据
	 * @param page
	 * @param prod
	 * @return
	 */
	IPage<Prod> findByPage(Page<Prod> page, Prod prod);

	/**
	 *  通过商品的id 查询商品的名称和商品的图片
	 * @param relation
	 * @return
	 */
	Prod getProdPicAndProdName(Long id);

	/**
	 * 分页查询prod 里面需要导入的数据
	 * @param page
	 * @param size
	 * @return
	 */
	List<ProdSolrDto> listProd(int page, int size,Date t1,Date t2);

	/**
	 * 得到一个商品的总条数
	 * @return
	 */
	int countProd(Date t1,Date t2);

	void decsStock(List<ShopCartItem> cartItems);

}
