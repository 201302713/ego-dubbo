package com.sxt.service;

import com.sxt.entity.ProdProp;
import com.sxt.entity.ProdPropValue;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdPropService extends IService<ProdProp> {

	/**
	 * 分页查询商品规格的数据
	 * @param page
	 * @param prodProp
	 * @return
	 */
	IPage<ProdProp> findByPage(Page<ProdProp> page, ProdProp prodProp);

	/**
	 * 根据属性的id 查询属性的值的集合
	 * @param id
	 * @return
	 */
	List<ProdPropValue> listPropValues(Long id);

}
