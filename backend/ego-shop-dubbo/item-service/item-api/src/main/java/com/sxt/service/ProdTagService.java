package com.sxt.service;

import com.sxt.entity.ProdTag;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分组表 服务类
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
public interface ProdTagService extends IService<ProdTag> {

	/**
	 * 分页查询标签数据
	 * @param page
	 * @param prodTag
	 * @return
	 */
	IPage<ProdTag> findByPage(Page<ProdTag> page, ProdTag prodTag);

	/**
	 * 加载所有的商品标签
	 * @return
	 */
	List<ProdTag> listAllProdTag();

}
