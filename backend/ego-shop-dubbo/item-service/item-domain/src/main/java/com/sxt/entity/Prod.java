package com.sxt.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Prod对象", description="商品")
//@TableName("`prod`") // ${tableName}
public class Prod implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品ID")
    @TableId(value = "prod_id", type = IdType.AUTO)
    private Long prodId;

    @ApiModelProperty(value = "商品名称")
    private String prodName;

    @ApiModelProperty(value = "店铺id")
    private Long shopId;
   // 若商品没有sku ，则该价格有作用，若有sku ，则以sku 为主
    @ApiModelProperty(value = "原价")
    private BigDecimal oriPrice;
    // 若商品没有sku ，则该价格有作用，若有sku ，则以sku 为主
    @ApiModelProperty(value = "现价")
    private BigDecimal price;

    @ApiModelProperty(value = "简要描述,卖点等")
    private String brief;

    @ApiModelProperty(value = "详细描述")
    private String content;

    @ApiModelProperty(value = "商品主图")
    private String pic;

    @ApiModelProperty(value = "商品图片，以,分割")
    private String imgs;

    // Mybatis 里有${}   
    // select prod_id from ${tableName} where status = #{status}
    // mybatis 会先进行替换 select prod_id from prod where status = ?
 // 若我们执行删除操作，他会自动删除操作，把delete->update set status=-1 // 以后我们查询值 会拼接 status=1
    @ApiModelProperty(value = "默认是1，表示正常状态, -1表示删除, 0下架")
    // 不能直接在value里面写1 or status = 0 ，因为以后Mybaits-plus 都会拼接 or status = 0，导致查询有问题
    // 因为假删除的sql 拼接是拼接在最前面 
//    @TableLogic(delval = "-1",value = "o")  若要搞假的删除，只能一个字段里面有2  个 1/0
    private Integer status;

    @ApiModelProperty(value = "商品分类")
    private Long categoryId;

    @ApiModelProperty(value = "销量")
    private Integer soldNum;

    @ApiModelProperty(value = "总库存")
    private Integer totalStocks;

    @ApiModelProperty(value = "配送方式json见TransportModeVO")
    private String deliveryMode;

    @ApiModelProperty(value = "运费模板id")
    private Long deliveryTemplateId;

    @ApiModelProperty(value = "录入时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "上架时间")
    private LocalDateTime putawayTime;

    @ApiModelProperty(value = "版本 乐观锁")
    private Integer version;
    
    @ApiModelProperty(value = "该商品里面的sku信息")
    @TableField(exist = false)
    private List<Sku> skuList = new ArrayList<Sku>(0) ; // 给个空的集合，可以避免前端的null 
    
    @ApiModelProperty(value = "该商品里面的分组信息")
    @TableField(exist = false)
    private List<Long> tagList = new ArrayList<Long>(0) ;
    
    @ApiModelProperty(value = "该商品配送信息")
    @TableField(exist = false)
    private DeliveryModeVo deliveryModeVo ;
    
    @Data
    public class DeliveryModeVo implements Serializable{
		private static final long serialVersionUID = 1L;
		private boolean hasShopDelivery ;
    	private boolean hasUserPickUp;
    }


}
