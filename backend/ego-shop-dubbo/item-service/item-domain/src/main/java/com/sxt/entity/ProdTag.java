package com.sxt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分组表
 * </p>
 *
 * @author liangtiandong
 * @since 2019-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ProdTag对象", description="商品分组表")
public class ProdTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分组标签id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "分组标题")
    private String title;

    @ApiModelProperty(value = "店铺Id")
    private Long shopId;

    @ApiModelProperty(value = "状态(1为正常,0为删除)") // status=1
//    @TableLogic(delval = "0",value = "1") // 逻辑删除的关键 发现前端该值的作用时禁用和启用，并不是删除操作
    private Integer status;

    @ApiModelProperty(value = "默认类型(0:商家自定义,1:系统默认)")
    private Boolean isDefault;

    @ApiModelProperty(value = "商品数量")
    private Long prodCount;

    @ApiModelProperty(value = "列表样式(0:一列一个,1:一列两个,2:一列三个)")
    private Integer style;

    @ApiModelProperty(value = "排序")
    private Integer seq;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除时间")
    private LocalDateTime deleteTime;


}
