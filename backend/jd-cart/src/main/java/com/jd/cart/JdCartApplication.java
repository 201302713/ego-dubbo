package com.jd.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdCartApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdCartApplication.class, args);
	}

}
