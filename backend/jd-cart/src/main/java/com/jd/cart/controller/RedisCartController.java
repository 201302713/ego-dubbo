package com.jd.cart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 使用redis 实现购物车
 * @author WHSXT-LTD
 *
 */
@RestController
@RequestMapping("/redis")
public class RedisCartController {
 
	@Autowired
	private StringRedisTemplate redisTemplate ;
	/**
	 * 购物车的标识可以来自cookie，但是cookie 有的东西不支持（微信小程序）
	 * 使用头来发送标识
	 * 1 在pc 的浏览器
	 *    a：pc的浏览器里面使用cookie 存储该标识
	 *    b: pc的浏览器发请求，在cookie 取出该标识，将该标识放在发送的头里面
	 * 2 微信小程序里面
	 *    a：在小程序有个storage，他也可以存储标识
	 *    b: 在storage 取标识，在头里面添加标识发送
	 * @param skuId
	 * @param count
	 * @return
	 */
	@GetMapping("/add/cart")
	public ResponseEntity<String> addCart(@RequestHeader("CART-LABEL")String label,String skuId,Integer count){
		String cartInfo = label +":cart-info" ;
		String cartTotal = label + ":cart-total" ;
		/**
		 * 使用redis 实现购物车的处理
		 */
		/**
		 * 1String:key:我们使用key 来区分不同的浏览器，-> 本质就是为了区分用户
		 * 2Object：SkuId
		 * 3Object：count
		 */
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		opsForValue.increment(cartTotal, count) ; // 使用该key 来记录用户添加的总条数
		HashOperations<String, Object, Object> opsForHash = redisTemplate.opsForHash();
		Boolean hasSkuId = opsForHash.hasKey(cartInfo, skuId); // 直接看该用户购物车里面是否有该商品
		if(hasSkuId) {
			String lastCountNum = opsForHash.get(cartInfo, skuId).toString(); // 获取之前的数量
			count += Integer.valueOf(lastCountNum) ;//给之前的数量里面，添加现在的数量
		}
		opsForHash.put(cartInfo, skuId, count+""); // 每次都是覆盖的设置
		
		return ResponseEntity.ok("条件成功");
	}
	/**
	 * redis的实现
	 * @param label
	 * @return
	 */
	@GetMapping("/cart/total")
	public ResponseEntity<Integer> getTotal(@RequestHeader("CART-LABEL")String label){
		String cartTotal = label +":cart-total" ;
		Boolean isHasKey = redisTemplate.hasKey(cartTotal);
		if(!isHasKey) {
			return ResponseEntity.ok(0);
		}
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		String totalCount = opsForValue.get(cartTotal);
		return ResponseEntity.ok(Integer.valueOf(totalCount));
	}
}
