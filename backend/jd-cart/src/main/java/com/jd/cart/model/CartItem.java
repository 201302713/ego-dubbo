package com.jd.cart.model;

public class CartItem {
	/**
	 * 该条目对应的具体商品
	 */
	private Long skuId ;
	/**
	 * 该条目的数量
	 */
	private Integer count ;
	
	public Long getSkuId() {
		return skuId;
	}
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}


}
